/**
 * The `vendor_files` property in Gruntfile.js holds files to be automatically
 * concatenated and minified with our project source files.
 *
 * NOTE: Use the *.min.js version when compiling for production.
 * Otherwise, use the normal *.js version for development
 *
 */

module.exports = {
    js: [
      // utility libraries
      'vendor/jquery/dist/jquery.min.js',

      // Angular components
      'vendor/angular/angular.js',
      'vendor/angular-messages/angular-messages.min.js',
      'vendor/angular-ui-router/release/angular-ui-router.min.js',
      'vendor/angular-resource/angular-resource.min.js',
      'vendor/angular-mocks/angular-mocks.js',
      'vendor/angular-notify/dist/angular-notify.min.js'

      // Angular Material
      'vendor/angular-animate/angular-animate.min.js',
      'vendor/angular-aria/angular-aria.min.js',
      'vendor/angular-material/angular-material.min.js',
      'vendor/angular-material-icons/angular-material-icons.min.js',

      // Icon Morph
      'vendor/svg-morpheus/compile/minified/svg-morpheus.js'

      // Local storage
      'vendor/angular-local-storage/dist/angular-local-storage.min.js',

      // Modernizer
      'vendor/modernizr/modernizr.js',

      // ngCordova
      'vendor/ng-cordova/dist/ng-cordova-mocks.min.js'
      'vendor/ng-cordova/dist/ng-cordova.min.js'

      //ng-webSql
      'vendor/ng-websql/dist/ng-websql.min.js'
    ],
    css: [ ],
    assets: [ ]
};
