angular.module('rpg-initiative-tracker', [
	/**
	 * Dependencies must be injected in specific order:
	 * 1) AngularJS dependencies
	 * 2) Compiled HTML templates
	 * 3) Common Services, Directives, Filters and Utilities
	 * 4) Main App Layout
	 * 5) Other App components (e.g. Toolbar, About, etc)
	 */

	// AngularJS dependencies
	'ngAnimate',
	'ngAria',
	'ngMaterial',
	'ngMdIcons',
	'ngMessages',
	'ui.router',
	'ngResource',
	'ngWebSql',

	// Include compiled HTML templates
	'templates',

	// Common/shared code
	'app.shared',

	// Layout
	'app.error',
	'app.loader',
	'app.layout',

	// Components
	'app.toolbar',
	'app.sidenav',
	'app.party',
	'app.template',
	'app.encounter'
])

.controller('MainCtrl', function ($scope){
	$scope.appName = 'rpg-initiative-tracker';
})

.config(function($mdThemingProvider, $locationProvider) {
	$locationProvider.hashPrefix('');
	var MyRedMap = $mdThemingProvider.extendPalette('red', {
		'A400': '#FFAB00'
	});
	$mdThemingProvider.definePalette('altRed', MyRedMap);
	$mdThemingProvider.theme('default')
		.primaryPalette('green')
		.accentPalette('blue', {
			'default': '300',
			'hue-1': '900'
		})
		.warnPalette('altRed', {
			'hue-1': 'A400',
			'hue-2': 'A700'
		});
})

.run(['Db', 'Preferences', '$state', function (Db, Preferences, $state) {
	$state.go('loader');
	Db.init().then(
		function(){
			Preferences.init().then(
				function(prefInit){
					$state.go('party');
				}, 
				function(prefErr){
					$log.error('Preferences.init failed');
				}
			);
			// $state.go('profile');
		}, 
		function(error){
			$state.go('error', {message:'Errore durante l\'inizializzazione del DB', subMsg: ''});
		}
	);
}]);
