angular.module('error.controllers.ErrorCtrl', [])
	.controller('ErrorCtrl', function($scope, $log, $stateParams) {
		$log.debug($stateParams);
		$scope.message = $stateParams.message;
		$scope.subMessage = $stateParams.subMsg;
	});
