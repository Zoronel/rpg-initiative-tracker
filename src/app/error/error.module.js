angular.module('app.error', [
	'ui.router',
	'error.controllers.ErrorCtrl'
])

.config(function config($stateProvider) {
	$stateProvider.state('error', {
		url: '/error/:message&:subMsg',
		templateUrl: 'error/error.view.html',
		controller: 'ErrorCtrl',
		title: 'Errore'
	});
});
