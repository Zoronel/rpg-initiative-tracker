angular.module('party.controllers.PartyCtrl', [])
	.controller('PartyCtrl', function($scope, $state, $log, $mdDialog, Utils, PartyServ, Db) {
		$scope.partyList;

		$scope.selectItem = function(item) {
			$state.go('party_details', {id: item.id, title_extra: '\''+item.name+'\''});
		}
		
		$scope.contextMenuVoices = defineContextMenuVoice();
		function defineContextMenuVoice(){
			var newBattle = new ContextMenuItem('Crea Incontro', 'colorize', 'battle');
			var voices = Utils.getDefContextMenuVoice();
			voices.push(newBattle);
			return voices;
		}
		$scope.contextMenuClick = function(ThatParty, item){
			switch(item.code){
				case '0':
				case 0:
					var messaggio = [
						{
							line:0,
							label: 'Numero Membri' ,
							text: ThatParty.countMember
						},
						{
							line:1,
							label: 'Descrizione',
							text: ThatParty.description
						},
						{
							line:2,
							label: 'Creato il' ,
							text: ThatParty.creation
						},
						{
							line:3,
							label: 'Ultma modifica' ,
							text: ThatParty.modify
						}
					];
					Utils.showMultilineInfo('Dettagli del party \''+ThatParty.name+'\'', messaggio);
					break;
				case '1':
				case 1:
					var modDialog = Utils.showObjGestDialog(ThatParty, 'pages/party/party.form.view.html');
					modDialog.then(
						function(PartyToUpdate){
							PartyToUpdate.save().then(
								function(result){
									if (result.esito) {
										Utils.showInfo('Modifica avvenuta con successo!');
									}else{
										Utils.showWarning('Modifica Fallita');
									}
								}, 
								function(result){
									Utils.showError('Errore durante la modifica', result);
								}
							);
						},
						function(){
							$log.debug('update aborted');
						}
					);
					break;
				case '2':
				case 2:
					var delDialog = Utils.showConfirm('Sei sicuro di voler eliminare questo party?', 'Non sarà possibile tornare indietro. In ogni caso i template rimarrano memorizzati');
					delDialog.then(
						function(){
							ThatParty.cancella().then(
								function(result){
									if (result.esito) {
										PartyServ.removeParty(ThatParty);
										$scope.partyList = PartyServ.getPartyList();
										Utils.showInfo('Cancellazione avvenuta con successo!');
									}else{
										Utils.showWarning('Cancellazione Fallita');
									}
								}, 
								function(result){
									Utils.showError('Errore durante la cancellazione', result);
								}
							);
						},
						function(){
							$log.debug('delete aborted');
						}
					);
					break;
				case 'battle':
					Utils.showInfo('COMING SOON');
					break;
				default:
					Utils.showError('Nessuna azione trovata per questa voce di menu', item);
					break;
			}
		}

		$scope.init = function(){
			PartyServ.ready().then(
				function(ready){
					if (ready) {
						$scope.partyList = PartyServ.getPartyList();
					}
				}
			);
		}

		$scope.addParty = function(){
			$log.debug('start add party');
			var AppCtnx = new ApplicationContext(Db, Utils);
			var newParty = new Party(AppCtnx);
			var insDialog = Utils.showObjGestDialog(newParty, 'pages/party/party.form.view.html');
			insDialog.then(
				function(answer){
					$log.debug(answer)
					PartyServ.addParty(answer);
				},
				function(){
					$log.debug('insert aborted');
				}
			);
		}
	});
