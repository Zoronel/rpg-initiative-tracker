angular.module('party.services.PartyServ', [])
	.factory('PartyServ', function($log, Utils, $state, Db) {
		var sv = this;
		sv.partys = Array();

		var services = {
			ready: ready,
			getPartyList: getPartyList,
			getParty: getParty,
			getMemberList: getMemberList,
			getMember: getMember,
			addParty: addParty,
			removeParty: removeParty,
			addMember: addMember
		};
		return services;

		function ready(){
			if (Utils.isBlank(sv.partys)) {
				var q = 'SELECT * FROM party';
				var AppCtnx = new ApplicationContext(Db, Utils);
				AppCtnx.addCore('$log', $log);
				return Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new Party(AppCtnx, row);
							if (Utils.isValidId(p.id)){
								this.push(p); //
							}
						}, sv.partys);
						return true;
					},
					function(error){
						$log.error(error);
						sv.partys = Array();
						return false;
					}
				);
			}else{
				return Promise.resolve(true);
			}
		}

		function getPartyList(){
			/*if (Utils.isBlank(sv.partys)) {
				var q = 'SELECT * FROM party';
				var AppCtnx = new ApplicationContext(Db, Utils);
				AppCtnx.addCore('$log', $log);
				Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new Party(AppCtnx, row);
							if (Utils.isValidId(p.id)){
								this.push(p); //
							}
						}, sv.partys);
					},
					function(error){
						$log.error(error);
						sv.partys = Array();
					}
				);
			}*/
			return sv.partys;
		}

		function getParty(id){
			if (Utils.isBlank(sv.partys)) return null;
			if (Utils.isBlank(id)) return null;

			var res = sv.partys.filter(function(v, i) {
				if (v.id == id) {
					return true;
				}
			});

			if (!Utils.isBlank(res)) {
				if (res.length > 1) {
					Utils.showError('Troppi party trovati con questo id: '+id, res).then(function() {
						$state.go('party');
					});
				} else {
					return res[0];
				}
			} else {
				Utils.showWarning('Nessun party trovato con id '+id).then(function() {
					$state.go('party');
				});
				return null;
			}
		}

		function getMemberList (id) {
			if (Utils.isBlank(sv.partys)) return null;
			if (Utils.isBlank(id)) return null;
			var thisParty = getParty(id);
			if (Utils.isBlank(thisParty)) return null;
			return thisParty.Members;
		}

		function getMember (partyId, memberId){
			if (Utils.isBlank(sv.partys)) {
				$log.debug('partys is isBlank');
				return null;
			}
			if (!Utils.isValidId(partyId)) {
				$log.debug('partyId is invalid - '+partyId);
				return null;
			}
			if (!Utils.isValidId(memberId)) {
				$log.debug('memberId is invalid - '+memberId);
				return null;
			}
			var thisParty = getParty(id);
			if (Utils.isBlank(thisParty)) {
				$log.debug('thisParty isBlank');
				return null;
			}
			return thisParty.getMember(memberId);
		}

		function addParty(NewParty){
			if (Utils.isBlank(NewParty)) {
				Utils.showError('Nessun oggetto trovato', NewParty);
			}
			NewParty.save().then(
				function(response){
					$log.debug(response);
					if (response.esito) {
						sv.partys.push(NewParty);
					}else{
						Utils.showError(response.messaggio);
					}
				},
				function(error){
					Utils.showError('Errore inatteso. Impossibile aggiugnere');
					$log.error(error);
				}
			);		
		}

		function removeParty(ThatParty){
			var holder = sv.partys.filter(function(pa){
				return pa.id != ThatParty.id;
			});
			sv.partys = holder;
		}

		function addMember(partyId, NewMember){
			if (Utils.isBlank(partyId) || !Utils.isValidId(partyId)) {
				Utils.showError('Id del party non valido', partyId);
				return false;
			}
			if (Utils.isBlank(NewMember)) {
				Utils.showError('Nessun oggetto trovato', NewMember);
				return false;
			};
			/*to add db gest
				if (!NewMember.save()){
					return false;
				}
			*/
			return getParty(partyId).addMember(NewMember);
		}
	});
