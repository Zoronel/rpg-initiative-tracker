angular.module('app.party', [
	'ui.router',
	'party.controllers.PartyCtrl',
	'party.controllers.PartyDetailsCtrl',
	'party.services.PartyServ'
])

.config(function config($stateProvider) {

	$stateProvider.state('party', {
		url: '/party',
		templateUrl: 'pages/party/party.view.html',
		controller: 'PartyCtrl',
		title: 'Lista dei Party'
	});

	$stateProvider.state('party_details', {
		url: '/party/:id',
		templateUrl: 'pages/party/party_details.view.html',
		controller: 'PartyDetailsCtrl',
		title: 'Dettaglio Party',
		backState: 'party'
	});
});
