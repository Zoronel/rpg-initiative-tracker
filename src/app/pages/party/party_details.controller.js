angular.module('party.controllers.PartyDetailsCtrl', [])
	.controller('PartyDetailsCtrl', function($scope, $log, $state, $stateParams, $timeout, PartyServ, TemplateServ, Utils) {
		$scope.selectedParty;
		$scope.filteredTemplateList;
		$scope.ready;
		$scope.contextMenuVoices = defineContextMenuVoice();
		function defineContextMenuVoice(){
			var voices = [
				new ContextMenuItem('Informazioni', 'info_outline', 0),
				new ContextMenuItem('Rimuovi', 'delete', 1)
			];
			return voices;
		}
		$scope.contextMenuClick = function(thatMember, item){
			switch(item.code){
				case '0':
					var messaggio = [
						{
							line:0,
							label: 'Tipologia di Creatura' ,
							text: thatMember.typeObj.description
						},
						{
							line:1,
							label: 'Modificatore Iniziativa' ,
							text: thatMember.init_mod
						},
						{
							line:2,
							label: 'Punti Ferita Massimi',
							text: thatMember.hp
						},
						{
							line:3,
							label: 'Classe Armatura',
							text: thatMember.ca
						},
						{
							line:4,
							label: 'Note',
							text: thatMember.note
						},
						{
							line:5,
							label: 'Creato il' ,
							text: thatMember.creation
						},
						{
							line:6,
							label: 'Ultma modifica' ,
							text: thatMember.modify
						}
					];
					Utils.showMultilineInfo('Dettagli della Creatura \''+thatMember.name+'\'', messaggio);
				case 0:
					break;
				case '1':
				case 1:
					var delDialog = Utils.showConfirm('Sei sicuro di voler rimuovere questo membro?', 'La rimozione di questo membro, non comporta la perdita del template');
					delDialog.then(
						function(){
							$scope.selectedParty.removeMember(thatMember);
						},
						function(){
							$log.debug('delete aborted');
						}
					);
					break;
				default:
					Utils.showError('Nessuna azione trovata per questa voce di menu', item);
					break;
			}
		};

		$scope.init = function(){
			$scope.ready = false;

			$log.debug('init() start');
			PartyServ.ready().then(
				function(ready){
					$log.debug('PartyServ.ready: ' + ready);
					if (ready) {
						$scope.selectedParty = PartyServ.getParty($stateParams.id);
					}
				}
			);
			TemplateServ.ready().then(
				function(ready){
					$log.debug('TemplateServ.ready: ' + ready);
					$timeout(function(){
						if (ready) {
							$scope.filteredTemplateList = TemplateServ.filteredTemplateList('PC', 'ANPC');
						}
						$scope.ready = true;
					}, 500);
				}
			);
		}

		$scope.addMember = function(){
			// debugger;
			if (Utils.isBlank($scope.filteredTemplateList)) return;
			var dataSet = $scope.filteredTemplateList.filter(function(v, i) {
				if(!$scope.selectedParty.hasMember(v)){
					return true;
				}
			});
			if (Utils.isBlank(dataSet)) {
				if (Utils.isBlank($scope.filteredTemplateList)) {
					var confirm = Utils.showConfirm('Attenzione', 'Nessun template trovato su db. Si desidera crearne uno?', 'Si', 'No');
					confirm.then(
						function(){
							$state.go('template');
						}, 
						function(){
							$log.debug('no template to use');
						}
					);
				}else{
					Utils.showInfo('Nessun template valido trovato. I membri di un party possono essere solo  PC o ANPC');
				}
			}else{
				var dialog = Utils.showDialogMultichose('Scegli i membri da aggiugnere', dataSet, false);
				dialog.then(
					function(selected){
						$scope.selectedParty.addMember(selected);
						$log.debug($scope.selectedParty);
					},
					function(){
						$log.debug('insert aborted');
					}
				);
			}
		};
	});
