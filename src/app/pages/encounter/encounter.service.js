angular.module('encounter.services.EncounterServ', [])
	.factory('EncounterServ', function($log, Utils, $state, Db) {
		var sv = this;
		sv.scontri = Array();

		var services = {
			ready: ready,
			getEncounterList: sv.scontri,
			getEncounter: getEncounter,
			addEncounter: addEncounter,
			removeEncounter: removeEncounter
		};
		return services;

		function ready(){
			if (Utils.isBlank(sv.scontri)) {
				var q = 'SELECT * FROM incontro';
				var AppCtnx = new ApplicationContext(Db, Utils);
				AppCtnx.addCore('$log', $log);
				return Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new Incontro(AppCtnx, row);
							if (Utils.isValidId(p.id)){
								this.push(p); //
							}
						}, sv.scontri);
						return true;
					},
					function(error){
						$log.error(error);
						sv.scontri = Array();
						return false;
					}
				);
			}else{
				return Promise.resolve(true);
			}
		}
	});