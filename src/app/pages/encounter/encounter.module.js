angular.module('app.encounter', [
	'ui.router',
	'encounter.controllers.EncounterCtrl',
	'encounter.services.EncounterServ'
])

.config(function config($stateProvider) {
	$stateProvider.state('encounter', {
		url: '/encounter',
		templateUrl: 'pages/encounter/encounter.view.html',
		controller: 'EncounterCtrl',
		title: 'Lista degli Incontri'
	});
});
