angular.module('app.template', [
	'ui.router',
	'template.controllers.TemplateCtrl',
	'template.controllers.TemplateDetailsCtrl',
	'template.services.TemplateServ'
])

.config(function config($stateProvider) {
	$stateProvider.state('template', {
		url: '/template',
		templateUrl: 'pages/template/template.view.html',
		controller: 'TemplateCtrl',
		title: 'Lista delle Creature'
	});

	$stateProvider.state('template_details', {
		url: '/template/:id',
		templateUrl: 'pages/template/template_details.view.html',
		controller: 'TemplateDetailsCtrl',
		title: 'Dettaglio Creatura',
		backState: 'template'
	});
})

.constant('BASE_TEM_TYPE', [
		{
			name:'PC',
			description:'Personaggio giocante'
		},
		{
			name:'ANPC',
			description:'Personaggio non giocante alleato'
		},
		{
			name:'ENPC',
			description:'Personaggio non giocante avversario'
		},
		{
			name:'MOB',
			description:'Mostro'
		},
		{
			name:'GEN',
			description:'Tipo generico'
		}
	]);
