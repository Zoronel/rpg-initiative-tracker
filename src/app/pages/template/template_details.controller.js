angular.module('template.controllers.TemplateDetailsCtrl', [])
	.controller('TemplateDetailsCtrl', function($scope, $log, TemplateServ, Utils, $stateParams) {
		$scope.selectedTemplate = TemplateServ.getTemplate($stateParams.id);
	});
