angular.module('template.services.TemplateServ', [])
	.factory('TemplateServ', function($log, Utils, $state, Db, BASE_TEM_TYPE) {
		var sv = this;
		sv.templates = Array();
		sv.temTypes = Array();
		initBaseTem();
		var services = {
			ready: ready,
			getTemplateList: getTemplateList,
			filteredTemplateList: filteredTemplateList,
			getTemplateTypeList: getTemplateTypeList,
			getTemplate: getTemplate,
			addTemplate: addTemplate,
			removeTemplate: removeTemplate
		};
		return services;

		function ready(){
			if (Utils.isBlank(sv.temTypes)) {
				var q = 'SELECT * FROM tipi_template';
				var AppCtnx = new ApplicationContext(Db, Utils);
				Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new TemplateType(AppCtnx, row);
							if (Utils.isValidId(p.id)){
								this.push(p);
							}
						}, sv.temTypes);
					},
					function(error){
						$log.error(error);
						sv.temTypes = Array();
					}
				);
			}
			if (Utils.isBlank(sv.templates)) {
				var q = 'SELECT * FROM templates';
				var AppCtnx = new ApplicationContext(Db, Utils);
				return Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new Template(AppCtnx, row);
							if (Utils.isValidId(p.id)){
								this.push(p); //
							}
						}, sv.templates);
						return true;
					},
					function(error){
						$log.error(error);
						sv.templates = Array();
						return false;
					}
				);
			}else{
				return Promise.resolve(true);
			}
		}
		function getTemplateList(){
			return sv.templates;
		}
		function filteredTemplateList(){
			var res;
			if (arguments.length > 0) {
				res = [];
				for (var i = 0; i < arguments.length; i++) {
					filterByType = arguments[i];
					if (typeof filterByType == 'number') {
						sv.templates.filter(function(v, i) {
							if (v.typeObj.id == filterByType) {
								res.push(v);
							}
						});
					}else if (typeof filterByType == 'string') {
						sv.templates.filter(function(v, i) {
							if (v.typeObj.name == filterByType) {
								res.push(v);
							}
						});
					}

				}
			}else{
				res = sv.templates;
			}
			return res;
		}

		function getTemplateTypeList(){
			return sv.temTypes;
		}

		function initBaseTem(){
			if (Utils.isBlank(sv.templates)) {
				var res = Db.insert('tipi_template', 3, BASE_TEM_TYPE);
				res.then(
					function(insRes){
						if (!insRes.esito) {
							Utils.showError(insRes.messaggio, insRes);
						};
					},
					function(insErr){
						if (!insRes.esito) {
							Utils.showError(insRes.messaggio, insRes);
						};
					}
				);
			}
		}

		function getTemplate(id){
			if (Utils.isBlank(sv.templates)) return null;
			if (Utils.isBlank(id)) return null;

			var res = sv.templates.filter(function(v, i) {
				if (v.id == id) {
					return true;
				}
			});

			if (!Utils.isBlank(res)) {
				if (res.length > 1) {
					Utils.showError('Troppi template trovati con questo id: '+id, res).then(function() {
						$state.go('template');
					});
				} else {
					return res[0];
				}
			} else {
				Utils.showWarning('Nessun template trovato con id '+id).then(function() {
					$state.go('template');
				});
				return null;
			}
		}

		function addTemplate(NewTemplate){
			if (Utils.isBlank(NewTemplate)) {
				Utils.showError('Nessun oggetto trovato', NewTemplate);
			}
			NewTemplate.save().then(
				function(response){
					$log.debug(response);
					if (response.esito) {
						sv.templates.push(NewTemplate);
					}else{
						Utils.showError(response.messaggio);
					}
				},
				function(error){
					Utils.showError('Errore inatteso. Impossibile aggiugnere');
					$log.error(error);
				}
			);		
		}

		function removeTemplate(ThatTemplate){
			var holder = sv.templates.filter(function(pa){
				return pa.id != ThatTemplate.id;
			});
			sv.templates = holder;
		}

	});
