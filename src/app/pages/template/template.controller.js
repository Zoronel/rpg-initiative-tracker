angular.module('template.controllers.TemplateCtrl', [])
	.controller('TemplateCtrl', function($scope, $state, $log, $mdDialog, Utils, TemplateServ, Db) {
		$scope.templateList;
		$scope.typeList;

		$scope.selectItem = function(item) {
			$state.go('template_details', {id: item.id, title_extra: '\''+item.name+'\''});
		}
		
		$scope.contextMenuVoices = defineContextMenuVoice();
		function defineContextMenuVoice(){
			var voices = Utils.getDefContextMenuVoice();
			/*var newBattle = new ContextMenuItem('Crea Incontro', 'colorize', 'battle');
			voices.push(newBattle);*/
			return voices;
		}

		$scope.contextMenuClick = function(ThatTemplate, item){
			$log.debug(item);
			switch(item.code){
				case '0':
				case 0:
					var messaggio = [
						{
							line:0,
							label: 'Tipologia di Creatura' ,
							text: ThatTemplate.typeObj.description
						},
						{
							line:1,
							label: 'Modificatore Iniziativa' ,
							text: ThatTemplate.init_mod
						},
						{
							line:2,
							label: 'Punti Ferita Massimi',
							text: ThatTemplate.hp
						},
						{
							line:3,
							label: 'Classe Armatura',
							text: ThatTemplate.ca
						},
						{
							line:4,
							label: 'Note',
							text: ThatTemplate.note
						},
						{
							line:5,
							label: 'Creato il' ,
							text: ThatTemplate.creation
						},
						{
							line:6,
							label: 'Ultma modifica' ,
							text: ThatTemplate.modify
						}
					];
					Utils.showMultilineInfo('Dettagli della Creatura \''+ThatTemplate.name+'\'', messaggio);
					break;
				case '1':
				case 1:
					var modDialog = Utils.showObjGestDialog(ThatTemplate, 'pages/template/template.form.view.html', $scope.typeList);
					modDialog.then(
						function(TemplateToUpdate){
							TemplateToUpdate.save().then(
								function(result){
									if (result.esito) {
										Utils.showInfo('Modifica avvenuta con successo!');
									}else{
										Utils.showWarning('Modifica Fallita');
									}
								}, 
								function(result){
									Utils.showError('Errore durante la modifica', result);
								}
							);
						},
						function(){
							$log.debug('update aborted');
						}
					);
					break;
				case '2':
				case 2:
					if (ThatTemplate.isUsed) {
						Utils.showWarning('Impossibile cancellare questa creatura. Risulta già usata in qualche party');
					}else{
						var delDialog = Utils.showConfirm('Sei sicuro di voler eliminare questa creatura?', 'Non sarà possibile tornare indietro');
						delDialog.then(
							function(){
								ThatTemplate.cancella().then(
									function(result){
										if (result.esito) {
											TemplateServ.removeTemplate(ThatTemplate);
											$scope.templateList = TemplateServ.getTemplateList();
											Utils.showInfo('Cancellazione avvenuta con successo!');
										}else{
											Utils.showWarning('Cancellazione Fallita');
										}
									}, 
									function(result){
										Utils.showError('Errore durante la cancellazione', result);
									}
								);
							},
							function(){
								$log.debug('delete aborted');
							}
						);
					}
					break;
				default:
					Utils.showError('Nessuna azione trovata per questa voce di menu', item);
					break;
			}
		}

		$scope.addTemplate = function(){
			$log.debug('start add template');
			var AppCtnx = new ApplicationContext(Db, Utils);
			var newTemplate = new Template(AppCtnx);
			var insDialog = Utils.showObjGestDialog(newTemplate, 'pages/template/template.form.view.html', $scope.typeList);
			insDialog.then(
				function(answer){
					$log.debug(answer)
					TemplateServ.addTemplate(answer);
				},
				function(){
					$log.debug('insert aborted');
				}
			);
		}

		$scope.init =function(){
			TemplateServ.ready().then(
				function(ready){
					$log.debug('TemplateServ.ready: ' + ready);
					$scope.templateList = TemplateServ.getTemplateList();
					$scope.typeList = TemplateServ.getTemplateTypeList();
				}
			);
		}
	});
