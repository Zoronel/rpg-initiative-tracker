angular.module('app.about', [
	'ui.router',
	'about.controllers.AboutCtrl'
])

.config(function config($stateProvider) {
	var name = 'about';
	$stateProvider.state(name, {
		url: '/'+name,
		templateUrl: name+'/'+name+'.view.html'
	});
});
