angular.module('sidenav.services.SideNav', [])
	.factory('SideNav', function(Utils) {

		// {
		//	 name: 'Nuovo Scontro',
		//	 icon: '',
		//	 route: 'new-battle'
		// }
		var menuItems = [
			//			('name', 'nome icona', 'route', 'funzione da eseguire')
			//									inserire una funzione previene il routing
			new MenuItem('Party', 'group', 'party'),
			new MenuItem('Creature', 'folder_shared', 'template'),
			new MenuItem('Incontri', 'colorize', 'encounter'),
			new MenuItem('Test Error', '', '', function(){
				Utils.showError('Test di errore');
			}),
			new MenuItem('Test Warning', '', '', function(){
				Utils.showWarning('Test di warning');
			}),
			new MenuItem('Test info', '', '', function(){
				Utils.showInfo('Test di informazione');
			})
		];

		var service = {
			getMenuItems: menuItems
		};
		return service;

	});
