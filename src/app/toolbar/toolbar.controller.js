angular.module('toolbar.controllers.ToolbarCtrl', [])
	.controller('ToolbarCtrl', function($log, $state, $scope, $mdSidenav) {
		var menuIcon = {
			name:'menu',
			action: function () {
				$mdSidenav('left').open();
			}
		}
		var backIcon = {
			name: 'arrow_back',
			action: function(){
				var current = $state.current;
				if (current.hasOwnProperty('backState')){
					$state.go(current.backState);
				}
			}
		}
		$scope.iconClick = function(){
			$scope.icon.action();
		}
		
		$scope.icon = menuIcon;
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (toState.hasOwnProperty('backState')) {
				$scope.icon = backIcon;
			}else{
				$scope.icon = menuIcon;		
			}
		});
	
		Object.defineProperty($scope, 'title', {
			get: function(){
				var current = $state.current;
				if (current.hasOwnProperty('title')){
					return current.title;
				}else{
					return 'Rpg Initiative Tracker';
				}
			}
		});
	});
