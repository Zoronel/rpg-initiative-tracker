angular.module('shared.directives.longClick', [])
	.directive('onLongClick', function($timeout) {
		return {
			restrict: 'A',
			link: function($scope, $elm, $attrs) {
				$elm.bind('mousedown', function(evt) {
					// Locally scoped variable that will keep track of the long press
					$scope.longPress = true;

					// We'll set a timeout for 600 ms for a long press
					$timeout(function() {
						if ($scope.longPress) {
							// If the touchend event hasn't fired,
							// apply the function given in on the element's on-long-press attribute
							$scope.$apply(function() {
								$scope.$eval($attrs.onLongClick)
							});
						}
					}, 600);
				});

				$elm.bind('mouseup', function(evt) {
					$scope.longPress = false;
					if ($attrs.onTouchEnd) {
						$scope.$apply(function() {
							$scope.$eval($attrs.onClickEnd)
						});
					}
				});
			}
		};
	})