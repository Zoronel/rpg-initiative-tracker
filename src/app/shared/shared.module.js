angular.module('app.shared', [
	//direttive
	'shared.directives.backButton',
	'shared.directives.longPress', //for touchscreen
	'shared.directives.longClick', //for mouse event

	//servizzi
	'shared.services.Utils',
	'shared.services.Preferences',
	'shared.services.Db'
])

.constant('DB_STRUCT', {
	name: 'rpg-initiative-tracker',
	logging: true,
	tables:[
		{
			name: 'party',
			columns: [
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'name', type: 'TEXT', is_null: false},
				{name: 'description', type: 'TEXT', is_null: true}
			],
			primary_key: [ 'id' ],
			uniques: [
				{
					fields: [ "name" ]
				}
			]
		},
		{
			name: 'templates',
			columns: [
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'name', type: 'TEXT', is_null: false},
				{name: 'type', type: 'INTEGER', is_null: false},
				{name: 'init_mod', type: 'INTEGER', is_null: true},
				{name: 'hp', type: 'INTEGER', is_null: true},
				{name: 'ca', type: 'INTEGER', is_null: true},
				{name: 'note', type: 'TEXT', is_null: true}

			],
			primary_key: ['id'],
			uniques: [
				{
					fields: ['name', 'type']
				}
			]		
		},
		{
			name: 'tipi_template',
			columns:[
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'name', type: 'TEXT', is_null: false},
				{name: 'description', type: 'TEXT', is_null: true}
			],
			primary_key: ['id'],
			uniques: [
				{
					fields: [ "name" ]
				}
			]
		},
		{
			name: 'party_templates',
			columns:[
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'party_id', type: 'INTEGER', is_null: false},
				{name: 'template_id', type: 'INTEGER', is_null: false},
			],
			primary_key: ['id'],
			uniques: [
				{
					fields: ['party_id', 'template_id']
				}
			]
		},
		{
			name: 'incontro',
			columns:[
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'title', type: 'TEXT', is_null: false}
			],
			primary_key: ['id']
		},
		{
			name: 'membri_incontro',
			columns:[
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'encunter_id', type: 'INTEGER', is_null: false},
				{name: 'template_id', type: 'INTEGER', is_null: false},
				{name: 'name', type: 'TEXT', is_null: false},
				{name: 'type', type: 'INTEGER', is_null: false, default: 1},
				{name: 'init_val', type: 'INTEGER', is_null: false, default:0},
				{name: 'total_hp', type: 'INTEGER', is_null: true, default:1},
				{name: 'damage', type: 'INTEGER', is_null: true, default:0},
				{name: 'ca', type: 'INTEGER', is_null: true, default:1},
				{name: 'status', type: 'INTEGER', is_null: true, default:-1},
				{name: 'note', type: 'TEXT', is_null: true}
			],
			primary_key: ['id'],
			uniques: [
				{
					fields: ['encunter_id', "name" ]
				}
			]
		},
		{
			name: 'status',
			columns:[
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'name', type: 'TEXT', is_null: false},
				{name: 'description', type: 'TEXT', is_null: true}
			],
			primary_key: ['id'],
			uniques: [
				{
					fields: [ "name" ]
				}
			]
		},
		{
			name: 'preferences',
			columns: [
				{name: 'id', type: 'INTEGER', is_null: false},
				{name: 'creation', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'modify', type: 'DATE', is_null: false, default: "datetime( 'now', 'localtime' )"},
				{name: 'field', type: 'TEXT', is_null: false},
				{name: 'value', type: 'TEXT', is_null: false},
				{name: 'tipo', type: 'TEXT', is_null: false, default: '\'STRING\''},
				{name: 'editable', type: 'BOOLEAN', is_null: false, default: 1}
			],
			primary_key: [ 'id' ],
			uniques: [
				{
					fields: ['field']
				}
			]
		}
	],
	views:[]

});
