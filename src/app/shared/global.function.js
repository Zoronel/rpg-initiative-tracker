function globalIsblank(mixed){
	if (mixed === null) return true;
	var type = typeof mixed;
	switch(type){
		case 'undefined':
			return true;
		case 'mixedect':
			if (mixed instanceof Array && mixed.length <=0) {
				return true;
			}
			break;
		case 'string':
			if(mixed.trim().length <= 0){
				return true;
			}
			break;
	}
	return false;
}

function parseBoolean(mixed){
	if (!globalIsblank(mixed)) {
		var type = typeof mixed;
		switch(type){
			case 'string':
				var tmp = mixed.toLowerCase();
				tmp.trim();
				tmp.replace(/\s+/g, '');
				if (tmp.match(/(true|vero|1){1}/g)) {
					return true;
				}else if (tmp.match(/(false|falso|0){1}/g)) {
					return false;
				}
				break;
			case 'number':
				if(mixed == 1){
					return true;
				}else if (mixed == 0) {
					return false;
				};
				break;
			case 'boolean':
				return mixed;
				break;
		}
	}
	return undefined;
}