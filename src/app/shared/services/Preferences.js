angular.module('shared.services.Preferences', [])
	.factory('Preferences', function Preferences($q, $log, $mdDialog, Utils, Db) {
		var self = this;
		self.preferences = [];
		self.ready = false;

		var tipi = [
			'STRING',
			'BOOLEAN',
			'NUMBER',
			'LONG',
			'INTEGER',
			'FLOAT',
			'DATE'
		];

		var service = {
			//ready: ready,
			init: init,
			set: set,
			get: get,
			reset: reset
		};
		return service;

		function init() {
			if (!self.ready) {
				var q = 'SELECT * FROM preferences';
				var ac = new ApplicationContext(Db, Utils);
				return Db.query(q).then(
					function(result){
						angular.forEach(result.rows, function(row){
							var p = new Preference(ac, row)
							self.preferences.push(p);
						});
						self.ready = true;
						$log.debug('preference correctly loaded', self.preferences);
						return true;
					},		
					function(error){
						Utils.showError('Errore nel caricamento elle preferenze', error);
						return false;
					}
				);
			}else{
				return Promise.resolve(true);
			}
		}

		function set(name, value){
			if (Utils.isBlank(name)) {
				Utils.showError('Nome della preferenza vuoto', name);
				return;
			};
			if (Utils.isBlank(value)) {
				Utils.showError('Valore della preferenza vuoto', value);
				return;
			};
			if (!self.ready) {
				Utils.showError('Servizio non pronto');
				return;
			};

			var type = typeof value;
			var pType;
			switch(type){
				case 'number':
					pType = 'NUMBER';
					break;
				case 'string':
					pType = 'TEXT';
					break;
				case 'boolean':
					pType = 'BOOLEAN';
					break;
				default:
					Utils.showError('Il tipo \''+type+'\' non è consentito', value);
					return;
			}
			if (Utils.isBlank(pType)){
				return;
			}
			var p;
			var isNew = false;
			if (Utils.isBlank(self.preferences) || (p = get(name)) == undefined) {
				var ac = new ApplicationContext(Db, Utils);
				p = new Preference(ac);
				isNew = true;
			};
			p.tipo = pType;
			p.field = name;
			p.value = value;

			p.save().then(
				function(qRes){
					if (qRes.esito) {
						if (isNew) {
							self.preferences.push(p);
						};
					}else{
						Utils.showError('Errore duranrte il salvataggio delle preferenze', qRes);
					}
				}, 
				function(qErr){
					Utils.showError(qErr.messaggio, qErr);
				}
			);
		}

		function get(name, returnVal, valIfNull){
			if ( (returnVal && Utils.isBlank(valIfNull)) || !returnVal) {
				valIfNull = undefined;
			};
			if (Utils.isBlank(name)) {
				Utils.showError('Nome della preferenza vuoto', name);
				return valIfNull;
			};
			
			if (!self.ready) {
				return valIfNull;
			};

			returnVal = parseBoolean(returnVal);
			if (angular.isUndefined(returnVal)) {
				returnVal = false;
			};
			var res = self.preferences.filter(function(pref){
				return pref.field == name;
			});
			if (res.length == 0){
				// $log.debug('preference \''+name+'\' not found');
				return valIfNull;
			}else if (res.length > 1){
				Utils.showError('Campo \''+name+'\' trovato più di una volta tra le preferenze', self.preferences);
				return valIfNull;
			} else {
				if (returnVal) {
					return res[0].value;
				}else{
					return res[0];
				}
			}
		}

		function reset(flushDb){
			flushDb = parseBoolean(flushDb);
			if (angular.isUndefined(flushDb)) {
				flushDb = false;
			};
			if (flushDb) {
				angular.forEach(self.preferences, function(value){
					value.cancella();
				});
				self.preferences = [];
			}else{
				self.preferences = [];
				init();
			}
		}
	});