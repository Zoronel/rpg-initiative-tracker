angular.module('shared.services.Utils', [])
	.factory('Utils', function Utils($log, $mdDialog, $http) {

		var service = {
			// general services
			isBlank: isBlank,
			extract: extract,
			getDefContextMenuVoice: getDefContextMenuVoice,
			getDefIfNull: getDefIfNull,
			isValidId: isValidId,
			nowTs: nowTs,
			nowStr: nowStr,
			nowObj: nowObj,
			objIsArrayLike: objIsArrayLike,
			// dialog services
			showError: showError,
			showWarning: showWarning,
			showInfo: showInfo,
			showMultilineInfo: showMultilineInfo,
			showConfirm: showConfirm,
			showObjGestDialog: showObjGestDialog,
			showDialogMultichose: showDialogMultichose,
			// specific functions
			wsCall: wsCall
		};
		return service;

		function isBlank(){
			var args = arguments;
			if (args.length == 0) return true;

			for(var i=0; i<args.length; i++){
				var obj = args[i];
				if (obj === null) return true;
				var type = typeof obj;
				switch(type){
					case 'undefined':
						return true;
					case 'object':
						if (obj instanceof Array && obj.length <=0) {
							return true;
						}else{
							var keyName = Object.getOwnPropertyNames(obj);
							if (keyName.length <= 0) return true;
						}
						break;
					case 'string':
						if(obj.trim().length <= 0){
							return true;
						}
						break;
					case 'number':
						if (isNaN(obj)) {
							return true;
						};
						break;
				}
			}

			return false;
		}

		function isValidId(integerNum){
			if (integerNum === null || integerNum === undefined) return false;
			if(typeof integerNum !== 'number'){
				if (typeof integerNum === 'string') {
					
					integerNum = parseInt(integerNum, 10);
					if (isNaN(integerNum)) return false;

				}else{
					return false;
				}
			}
			return integerNum > 0 ? true : false;
		}

		function nowTs(){
			var now = new Date();
			return now.getTime();
		}

		function nowStr(eng){
			if (isBlank(eng) || (eng = parseBoolean(eng)) == undefined) {
				eng = false;
			}
			var now = new Date();
			var str;
			try{
				if (eng) {
					str = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
				}else{
					str = now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear() + ' ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
				}
			}catch(e){
				$log.error(e);
				str = '';
			}
			return str;
		}

		function nowObj(){
			var now = new Date();
			return now;
		}


		function extract(haystack, key, value){
			if (isBlank(haystack, key, value)) return null;
			if (typeof haystack != 'object') return null;
			var straws = haystack.filter(function(straw){
				return straw[key] == value;
			});
			if (straws.length != 1){
				$log.debug(straws, key, value);
				return null;	
			} 
			return straws[0];
		}

		function getDefContextMenuVoice(){
			var voices = [
				new ContextMenuItem('Informazioni', 'info_outline', 0),
				new ContextMenuItem('Modifica', 'mode_edit', 1),
				new ContextMenuItem('Delete', 'delete', 2)
			];
			return voices;
		}

		function getDefIfNull(mixed, defIfNull){
			if (isBlank(mixed)) {
				return defIfNull;
			};
			return mixed;
		}

		function objIsArrayLike(obj){
			if (isBlank(obj)) return false;
			if (typeof obj != 'object') return false;
			if (obj instanceof Array) return true;
			if (obj.hasOwnProperty('lenght')) return true;
			var keyName = Object.getOwnPropertyNames(obj);
			if (keyName.length > 0) {
				for (var i = 0; i < keyName.length; i++) {
					var name = keyName[i];
					var testName = parseInt(name, 10);
					if (isNaN(testName)) return false;
				};
			};
			return true;
		}

		function showError(messaggio, log){
			if (!isBlank(log)) {
				$log.debug('Errorlog', log);
			};
			var msg;
			if (isBlank(messaggio)) {
				msg = 'Errore riscontrato';
			}else{
				msg = messaggio;
			}
			var title = 'Errore riscontrato';
			var dataSet = {message: msg};
			return $mdDialog.show({
				templateUrl: 'shared/views/dialog.error.view.html',
				controller: baseDialogController,
				clickOutsideToClose: true,
				fullscreen: false,
				multiple: true,
				locals:{
					title:title,
					dataSet:dataSet
				}
			});
		}

		function showWarning(messaggio){
			if (isBlank(messaggio)) {
				return showError('Nessun messaggio impostato', messaggio);
			}else{
				var title = 'Attenzione!';
				var dataSet = {message: messaggio};
				return $mdDialog.show({
					templateUrl: 'shared/views/dialog.warning.view.html',
					controller: baseDialogController,
					clickOutsideToClose: true,
					fullscreen: false,
					multiple: true,
					locals:{
						title:title,
						dataSet:dataSet
					}
				});
			}
		}

		function showInfo(messaggio){
			if (isBlank(messaggio)) {
				return showError('Nessun messaggio impostato', messaggio);
			}else{
				var title = 'Informazione';
				var dataSet = {message: messaggio};
				return $mdDialog.show({
					templateUrl: 'shared/views/dialog.info.view.html',
					controller: baseDialogController,
					clickOutsideToClose: true,
					fullscreen: false,
					multiple: true,
					locals:{
						title:title,
						dataSet:dataSet
					}
				});
			}
		}

		function showMultilineInfo(title, dataSet){
			if (isBlank(title)) title = 'Informazioni';
			if (isBlank(dataSet)){
				showError('Nessun dataSet passato alla funzione', dataSet);
			} 
			$log.debug(dataSet);
			$mdDialog.show({
				templateUrl: 'shared/views/dialog.multiline.view.html',
				controller: baseDialogController,
				clickOutsideToClose: true,
				fullscreen: false,
				locals:{
					title:title,
					dataSet:dataSet
				}
			});
		}

		function baseDialogController($scope, $mdDialog, title, dataSet){
			$scope.title = title;
			$scope.dataSet = dataSet;
			$scope.hide = function() {
				$mdDialog.hide();
			};
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
		}

		function showConfirm(title, message, okBtn, cancelBtn){
			if (isBlank(okBtn)) {
				okBtn = 'Si';
			};
			if (isBlank(cancelBtn)) {
				cancelBtn = 'Annulla';
			};
			if (isBlank(title, message)) {
				$log.debug(title, message);
				showError('Nessun titolo o messaggio impostato');
				return Promise.reject();
			}else{
				return $mdDialog.show(
					$mdDialog.confirm()
					.title(title)
					.textContent(message)
					.ariaLabel('Attenzione')
					.ok(okBtn)
					.cancel(cancelBtn)
				);
			}
		}

		function showObjGestDialog(newObject, templateUrl, extraData){
			return $mdDialog.show({
				controller: objGestDialogController,
				templateUrl: templateUrl,
				clickOutsideToClose: false,
				fullscreen: true,
				locals:{
					newObj: newObject,
					extraData: extraData
				}
			});
		}
		function objGestDialogController($scope, $mdDialog, newObj, extraData){
			$scope.newObj = newObj;
			$scope.extraData = extraData;

			$scope.hide = function() {
				$mdDialog.hide();
			};

			$scope.cancel = function() {
				$mdDialog.cancel();
			};

			$scope.answer = function(answer) {
				if (answer) {
					if (newObj.hasOwnProperty('upToDate') && newObj.upToDate) {
						$mdDialog.hide($scope.newObj);
					}else{
						$mdDialog.cancel();
					}
				}else{
					$mdDialog.cancel();
				}
			};
		}

		function showDialogMultichose(title, dataSet, confirmOnClick){
			if (isBlank(title)) title = 'Scelta multipla';
			if (isBlank(dataSet)){
				return showError('Nessun data set trovato!', dataSet);
			};
			if (isBlank(confirmOnClick) || typeof confirmOnClick != 'boolean') confirmOnClick = false;
			return $mdDialog.show({
				controller: dialogMultichoseController,
				templateUrl: 'shared/views/dialog.multichose.view.html',
				clickOutsideToClose: false,
				fullscreen: true,
				locals:{
					title: title,
					dataSet: dataSet,
					confirmOnClick: confirmOnClick
				}
			});
		}
		function dialogMultichoseController($scope, $mdDialog, title, dataSet, confirmOnClick){
			$scope.showCheckBox = !confirmOnClick;
			$scope.title = title;
			$scope.dataSet = dataSet;

			$scope.selected = [];

			$scope.toggle = function (item) {
				var idx = $scope.selected.indexOf(item);
				if (idx > -1) {
					$scope.selected.splice(idx, 1);
				} else {
					$scope.selected.push(item);
				}
			};

			$scope.isSelected = function (item) {
				return $scope.selected.indexOf(item) > -1;
			};

			$scope.itemClicked = function(item){
				$scope.selected.push(item);
				$mdDialog.hide($scope.selected);
			}

			$scope.hide = function() {
				$mdDialog.hide();
			};

			$scope.cancel = function() {
				$mdDialog.cancel();
			};

			$scope.confirm = function() {
				$mdDialog.hide($scope.selected);
				$scope.selected = [];
			};
		}

		function wsCall(Caller){
			$log.debug('wsCall: start');
			var res = new Response(false, '', null, 'Utils.wsCall');
			if (isBlank(Caller)) {
				res.messaggio = 'Caller non trovato';
				res.data = Caller;
				Promise.reject(res)
			};
			if (typeof Caller != 'object' || !(Caller instanceof WsCaller)) {
				res.messaggio = 'Caller non è un\'oggetto valido';
				res.data = Caller;
				Promise.reject(res);
			};
			var data = getDefIfNull(Caller.dataSet, {});
			var httpProm = $http({
				url: Caller.url,
				method: Caller.method,
				data: data,
				transformRequest: Caller.transformRequest,
				headers: Caller.headers
			});
			return httpProm.then(
				function(httpRes){
					$log.debug('wsCall: end', httpRes, res);
					if (httpRes.status == 200) {
						res.esito = true;
						res.data = httpRes.data;
						res.messaggio = 'Chiamata al servizio conclusa con successo';
					}else{
						res.esito = false;
						res.data = httpRes;
						res.messaggio = 'Il server non ha risposto come atteso.\n'+httpRes.statusText;
					}
					return res;
				},
				function(httpErr){
					res.esito = false;
					res.data = httpErr;
					res.messaggio = 'Chiamata al servizio fallita';
					return res;
				}
			);
		}

	});
