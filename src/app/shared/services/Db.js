//`
angular.module('shared.services.Db', [])
	.factory('Db', function Db($q, $db, $log, $mdDialog, Utils, DB_STRUCT) {
		var sv = this;
		sv.version = 1;
		sv.platform = 'browser';
		
		var ON_CONFLICT = [
			'ROLLBACK',
			'ABORT',
			'FAIL',
			'IGNORE',
			'REPLACE'
		];

		var COL_TYPE = [
			'TEXT',
			'LONG',
			'INTEGER',
			'BOOLEAN'
		]

		var service = {
			//ready: ready,
			init: init,
			recreate: recreate,
			get_tb: get_tb,
			tb_exist: tb_exist,
			get_colums: get_colums,
			column_exist: column_exist,
			isValidColType: isValidColType,
			query: query,
			insert: insert,
			update: update,
			cancella: cancella
		};
		return service;
		
		//var ready = $q.defer();

		function init(){
			var deferred = $q.defer();
			$log.debug(DB_STRUCT);
			if ($db.initialized) {
				deferred.resolve();
			}else{
				$db.init(DB_STRUCT).then(
					function(){
						$log.debug('Db is ready');
						deferred.resolve();					
					},
					function(error){
						Utils.showError('Errore durante l\'inizializzazione del DB', error);
						deferred.reject(error);
					}
				);
			}
			return deferred.promise;
		}		

		function recreate(){
			$db.recreate();
		}

		function query(plainText){
			var deferred = $q.defer();
			$db.query(plainText).then(
				function(result){
					$log.debug('query', plainText, result);
					deferred.resolve(result);
				},
				function(error){
					$log.debug(plainText);
					Utils.showError('Errore durante l\'eseguzione della query.', error);
					deferred.reject(error);
				}
			);
			return deferred.promise;
		}

		function get_tb(tb_name){
			var tbs = DB_STRUCT.tables.filter(function(tb){
				return tb.name == tb_name;
			});
			if (tbs.length != 1){
				return null;
			}else{
				return tbs[0];
			}
		}

		function tb_exist(tb_name){
			var tb = get_tb(tb_name);
			return !(tb == null);
		}

		function get_colums(tb_name){
			var tb = get_tb(tb_name);
			if (Utils.isBlank(tb)) return null;
			return tb.columns;
		}

		function column_exist(tb_name){			
			if (!tb_exist(tb_name)) return false;
			var tb = get_tb(tb_name);
			var res = true;
			var column_names = Array.prototype.slice.call(arguments, 1);
			if (column_names.length == 0) return false;

			for (var i = 0; i < column_names.length; i++) {
				column_name = column_names[i];

				var columns = tb.columns.filter(function(column){
					return column.name == column_name;
				});
				if (columns.length != 1) {
					$log.debug('colonna \''+column_name+'\' non trovata in \''+tb_name+'\'');
					res = false;
				}
			};
			return res;	
		}

		function isValidColType(colType){
			if (!Utils.isBlank(colType) && angular.isString(colType)) {
				var find = false;
				haystack = COL_TYPE;
				haystack.push('NUMBER');
				haystack.push('FLOAT');
				angular.forEach(haystack, function(value){
					if (colType == value) find = true;
				});
				return find;
			}else{
				return false;
			}
		}

		function is_system_col(col_name){
			if (Utils.isBlank(col_name)) return false;
			return col_name == 'id' || col_name == 'creation' || col_name == 'modify';
		}		

		function generate_dataset(tb_name, rows, keepid, optype){
			$log.debug('start generation of dataset');
			if (Utils.isBlank(keepid)) keepid = false;
			if (Utils.isBlank(optype)) optype = 'INSERT';
			if (Utils.isBlank(tb_name, rows)) {
				Utils.showError('Uno o più parametri mancanti');
				$log.debug(tb_name, rows);
				return null;
			}

			var tb = get_tb(tb_name);
			if (Utils.isBlank(tb)) return null;

			var tb_columns = tb.columns.filter(function(tbcol){
				if (keepid && tbcol.name == 'id'){
					return true;
				} else {
					return !is_system_col(tbcol.name);
				}
			});

			var cleaned_rows = Array();
			for (var i = 0; i < rows.length; i++) {
				new_row = rows[i];
				if (typeof new_row != 'object') continue;
				var die = false;
				angular.forEach(new_row, function(value, key){
					if (!die) {
						if (!column_exist(tb_name, key) || (keepid && is_system_col(key) && key != 'id') || (!keepid && is_system_col(key))) {
							delete new_row[key];
						}else {
							var col = Utils.extract(tb_columns, 'name', key),
								col_type = '';

							switch(col.type){
								default:
								case 'TEXT':
									col_type = 'string';
									break;
								case 'LONG':
								case 'INTEGER':
									col_type = 'number';
									break;
								case 'BOOLEAN':
									col_type = 'boolean';
									break;
							}
							if (typeof value != col_type){
								if (col_type == 'number' && typeof value == 'string') {
									if ( isNaN((value = parseInt(value, 10))) ) die=true;
								}else if (col_type == 'boolean') {
									if (typeof value == 'string') {
										value = value.trim();
										if (value.toLowerCase() == 'true' || value.toLowerCase() == 'vero' || value.toLowerCase() == '1') {
											value = true;
										}else if (value.toLowerCase() == 'false' || value.toLowerCase() == 'falso' || value.toLowerCase() == '0') {
											value = false;
										}else{
											die = true;
										}
									}else if (typeof value == 'number') {
										if (value == 0) {
											value = false;
										}else if (value == 1){
											value = true;
										}else{
											die = true;
										}
									};
								}else if (col_type == 'string') {
									value = value + '';
								}else{
									die = true;
								}							
							}
							if (die) {
								Utils.showError('Data type error. Valore trovato errato');
								$log.debug(key, value, (typeof value), col_type);
							}
							if (col.type == 'BOOLEAN') { //in SQLite Boolean == tinyint
								if(value == true){
									value = 1;
								}else{
									value = 0;
								}
							};
							if (value != new_row[key] || typeof value != typeof new_row[key]) {
								new_row[key] = value;
							};
						}
					}
				});

				if (die) return null; 
				if(optype == 'INSERT'){ 
					/*fill with null if col is nullable*/
					for (var y = 0; y < tb_columns.length; y++) {
						column = tb_columns[y];
						if(!new_row.hasOwnProperty(column.name)){
							if (column.is_null){
								new_row[column.name] = null;
							}else{
								$log.debug('Campo obbligatorio mancante', new_row, column);
								return null;
							}
						}
					}
				}
				cleaned_rows.push(new_row);
			}
			$log.debug('generation of dataset: compleate');
			return cleaned_rows;
		}

		function insert(tb_name, onConflict){
			$log.debug('insert into '+tb_name+': start');
			var result = new Response(false, '', undefined, 'Db.insert');
			$log.debug(result);
			var tb = get_tb(tb_name);
			if (Utils.isBlank(tb)){
				result.esito = false;
				result.messaggio = 'Nome tabella non valido';
				result.data = tb_name;
				return Promise.resolve(result);
			}
			
			var tb_columns = tb.columns.filter(function(tbcol){
				return !is_system_col(tbcol.name);
			});

			var slice = 1;
			var queryConflict = '';
			if (typeof onConflict === 'number'){
				slice++;
				if (onConflict > 4){
					onConflict = 1;
				}
				queryConflict = ' OR '+ON_CONFLICT[onConflict];
			}

			var rows;
			if (arguments.length == (slice + 1)) {
				if (arguments[slice] instanceof Array){
					rows = arguments[slice];
				}else if(Utils.objIsArrayLike(arguments[slice])) {
					rows = arguments[slice];
					var keyName = Object.getOwnPropertyNames(rows);
					rows.length = keyName.length;
				}else{
					rows = Array.prototype.slice.call(arguments, slice);
				}
			}else{
				rows = Array.prototype.slice.call(arguments, slice);
			}

			var dataset = generate_dataset(tb_name, rows, false);

			if (Utils.isBlank(dataset)) {
				result.esito = false;
				result.messaggio = 'Errore durante la creazione del Dataset';
				result.data = {
					arguments: arguments,
					dataset: dataset
				}
				return Promise.resolve(result);
			}else{

				result.data = {
					ok:0,
					fail:0,
					transactions:[]
				}
				var is_single = (dataset.length == 1);
				var promises = Array();

				var baseInsertQuery = 'INSERT'+queryConflict+' INTO '+tb_name;
				baseInsertQuery += ' (';
				var vals = Array();
				for (var i = 0; i < tb_columns.length; i++) {
					var column = tb_columns[i];
					if (i>0) baseInsertQuery += ', ';
					baseInsertQuery += column.name;
					var y = 0;
					for (var x = 0; x < dataset.length; x++) {
						var row = dataset[x];
						if (row.hasOwnProperty(column.name)){
							if (typeof vals[y] == 'undefined') vals[y] = Array();
							vals[y].push(row[column.name]);
						}
						y++;
					}
				}

				baseInsertQuery += ') VALUES ';
				
				angular.forEach(vals, function(value, key){
					var insertQuery = baseInsertQuery + '(';
					var tmp = Array();
					for(var y = 0; y < value.length; y++){
						var par = value[y];
						if (Utils.isBlank(par)) {
							par = 'NULL';
						}else if(typeof par == 'string'){
							par = par.trim();
							par = par.replace(/(")/g, '\'');
							par = '"'+par+'"';
						}else if (par instanceof Date) {
							var dTstr = '';
							dTstr += par.getFullYear()+'-';
							dTstr += par.getMonth()+'-';
							dTstr += par.getDate()+' ';
							dTstr += par.getHours()+':';
							dTstr += par.getMinutes()+':';
							dTstr += par.getSeconds();
							par = '"'+dTstr+'';
						};
						tmp.push(par);
					}

					insertQuery += tmp.join(', ')+')';

					var innerRes = new Response(false, '', undefined, 'Db.insert.'+key);
					result.data.transactions.push(innerRes);
					var prom = query(insertQuery);
					prom.then(
						function(qRes){
							innerRes.esito = true;
							innerRes.messaggio = 'Query di inserimento conclusa con successo';
							innerRes.data = {
								query:insertQuery,
								insertId: qRes.insertId,
								rowsAffected: qRes.rowsAffected
							}
							result.data.ok++;
							return innerRes;
						},
						function(qErr){
							innerRes.esito = false;
							innerRes.messaggio = 'Errore riscontrato durante l\'eseguzione della query';
							innerRes.data = {
								query:insertQuery,
								error : qErr
							}
							result.data.fail++;
							return innerRes;
						}
					);
					promises.push(prom);					
				});
				var deferred = $q.defer();
				$q.all(promises).then(
					function(res){						
						$log.debug(result);
						$log.debug('insert into '+tb_name+': compleate');
						if (result.data.ok == 0){
							result.esito = false;
							result.messaggio = 'Tutti gli insert sono falliti';
							deferred.reject(result);
						}else{
							result.esito = true;
							if (result.data.fail == 0){
								result.messaggio = 'Tutti gli insert hanno avuto successo';
							}else{
								result.messaggio = result.fail+' insert su '+rows.length+ ' non sono adati a buon fine';
							}							

							if (is_single) {
								deferred.resolve(result.data.transactions[0]);
							}else{
								deferred.resolve(result);
							}
						}
					},
					function(err){
						$log.debug(result);
						$log.error(err);
						result.esito = false;
						result.messaggio = 'Errore nella eseguzione multipla';
						deferred.reject(result);
					}
				);
				$log.debug('returning promise');
				return deferred.promise;
			}
		}

		function update(tb_name, onConflict){
			var deferred = $q.defer();
			var resultSet = {
				transactions:[],
				ok:0,
				fail:0
			}

			var results = new Response(false, '', resultSet, 'Db.update');
			
			$log.debug('start update into '+tb_name);
		
			var tb = get_tb(tb_name);
			if (Utils.isBlank(tb)){
				results.esito = false;
				results.messaggio = 'Nome tabella non valido';
				deferred.reject(results);
			}
			var tb_columns = tb.columns.filter(function(tbcol){
				return !is_system_col(tbcol.name);
			});
			
			var slice = 1;
			var queryConflict = '';
			if (typeof onConflict === 'number'){
				slice++;
				if (onConflict > 4){
					onConflict = 1;
				}
				queryConflict = ' OR '+ON_CONFLICT[onConflict];
			}

			var rows;
			if (arguments.length == (slice + 1)) {
				if (arguments[slice] instanceof Array){
					rows = arguments[1];
				}else{
					rows = Array.prototype.slice.call(arguments, slice);
				}
			}else{
				rows = Array.prototype.slice.call(arguments, slice);
			}

			var dataset = generate_dataset(tb_name, rows, true, 'UPDATE');

			if (Utils.isBlank(dataset)) {
				results.esito = false;
				results.messaggio = 'Errore durante la creazione del Dataset';
				deferred.reject(results);
			}else{
				var r = 0;				
				var promises = Array();
				angular.forEach(dataset, function(row_to_update){
					var updateQuery = '';
	
					var thisRes = new Response(false, '', r, 'Db.update');
					results.data.transactions.push(thisRes);
					
					if (!row_to_update.hasOwnProperty('id')) {
						thisRes.esito = false;
						thisRes.messaggio = 'Campo id non trovato nel dataset';
						thisRes.data = row_to_update;
						results.data.fail++;
					}else{
						var id = row_to_update.id;
						if (!Utils.isValidId(id)) {
							thisRes.esito = false;
							thisRes.messaggio = 'Campo id trovato nel dataset non valido';
							results.data.fail++;
							$log.debug(id);
						}else{
							updateQuery = 'UPDATE'+queryConflict+' '+tb_name+' SET ';
							if (column_exist(tb_name, 'modify')) {
								updateQuery += 'modify=datetime( \'now\', \'localtime\' )';
							};
							for (var i = 0; i < tb_columns.length; i++) {
								var column = tb_columns[i];
								if (row_to_update.hasOwnProperty(column.name)){
									var field = column.name;
									var val = row_to_update[column.name]
									if(typeof val == 'string'){
										val.replace(/(\')/g, '\\\'');
										val = '\''+val+'\'';
									}
									updateQuery += ', '+field+'='+val;
								}
							};
							updateQuery += ' WHERE id = '+id;
							var qProm = query(updateQuery).then(
								function(qRes){
									thisRes.esito = true;
									thisRes.messaggio = updateQuery;
									results.data.ok++;									
								},
								function(qErr){
									thisRes.esito = false;
									thisRes.messaggio = updateQuery;
									thisRes.data = qErr;
									results.data.fail++;
								}
							);
							promises.push(qProm);
						}
					}
					r++;
				});

				$q.all(promises).then(
					function(res){						
						$log.debug(results);
						if (results.data.ok == 0){
							results.esito = false;
							results.messaggio = 'Tutti gli update sono falliti';
							//Utils.showError('Tutti gli update sono falliti');
							deferred.reject(results);
						}else{
							results.esito = true;
							if (results.data.fail == 0){
								results.messaggio = 'Tutti gli update hanno avuto successo';
								//Utils.showInfo('Tutti gli update hanno avuto successo');
							}else{
								results.messaggio = results.fail+' update su '+rows.length+ ' non sono adati a buon fine';
								//Utils.showWarning(results.fail+' update su '+rows.length+ ' non sono adati a buon fine');
							}
							deferred.resolve(results);
						}
					},
					function(err){
						$log.debug(results);
						$log.error(err);
						results.esito = false;
						results.messaggio = 'Errore nella eseguzione multipla';
						deferred.reject(results);
					}
				);

			}
			return deferred.promise;
		}

		function cancella(tb_name){
			$log.debug('start delete into '+tb_name);

			var rows = Array.prototype.slice.call(arguments, 1);

			var result = new Response(false, '', null, 'Db.cancella');
			result.data = {
				tb: tb_name,
				args: rows,
				deleteQuery: '',
				rowsAffected: 0
			};

			if (Utils.isBlank(tb_name, rows)) {
				result.messaggio = 'Uno o più parametri mancanti';
				return Promise.reject(result);
			}else if(!tb_exist(tb_name)){
				result.messaggio = 'La tabella \''+tb_name+'\' non esiste';
				return Promise.reject(result);
			}else{
				var deleteQuery = 'DELETE FROM '+tb_name;
				var ids = Array();
				for (var i = 0; i < rows.length; i++) {
					id = rows[i];
					if (!Utils.isValidId(id)) continue;
					ids.push(id);
				};
				if (Utils.isBlank(ids)) {
					result.messaggio = 'Nessuno degli id passati risulta valido';
					return Promise.reject(result);
				};
				deleteQuery += ' WHERE id IN('+ids.join(', ')+')';
				result.data.deleteQuery = deleteQuery;
				$log.debug(result);
				return query(deleteQuery).then(
					function(qRes){
						result.esito = true;
						result.messaggio = "Cancellazione eseguita con successo";
						result.data.rowsAffected = qRes.rowsAffected;
						return result;
					},
					function(qErr){
						result.esito = false;
						result.messaggio = "Cancellazione fallita";
						result.data['SqlError'] = qErr;
						$log.error(qErr);
						return result;
					}
				);
			}
		}
	});