function Party(ApplicationContext, row){
	var self = this;
	self.Tb = 'party';
	
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}

	var _log = Ac.get('$log');

	self.id = -1;
	self.creation = '';
	self.modify = '';

	var holder = {
		id:-1,
		name: '',
		description: ''
	};
	self.upToDate = false;

	Object.defineProperty(this, 'name', {
		get: function() {
			return holder.name;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.name = tmp;
			self.upToDate = true;
		}
	});

	Object.defineProperty(this, 'description', {
		get: function() {
			return holder.description;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal;
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.description = tmp;
			self.upToDate = true;
		}
	});

	self.name;
	self.description;

	self.members = Array();

	if (row !== null && typeof row === 'object'){
		angular.forEach(row, function(val, prop){
			this[prop] = val;
		}, self);

		angular.forEach(holder, function(val, prop){
			this[prop] = self[prop];
		}, holder);
		
		var memProm = Ac.Db.query('SELECT * FROM party_templates WHERE party_id = '+self.id);
		memProm.then(
			function(qRes){
				if (qRes.rows.length > 0) {
					for (var i = 0; i < qRes.rows.length; i++) {
						var row = qRes.rows[i];					
						self.members.push(new Template(Ac, row.template_id));
						_log.debug(self.members);
					};
				};
			},
			function(qErr){
				Ac.Utils.showError('Errore durante il recupero dei membri', qErr);			
			}
		);
	}

	self.save = function(){
		var prom;
		var isNew = true;

		var r = new Response(false, '', this, 'Party.save');

		if (this.id > 0) {
			isNew = false;
		}

		if(Ac.Utils.isBlank(holder.name)) {
			r.esito = false;
			r.messaggio = 'Nome mancante';
			return Promise.resolve(r);
		}
		var dataSet = {
			id: self.id,
			name: self.name,
			description: self.description
		}
		// console.log(dataSet);
		if (isNew) {
			prom = Ac.Db.insert('party', dataSet);
			return prom.then(
				function(result){
					if (result.esito) {
						self.id = result.data.insertId;
						r.esito = true;
						r.messaggio = 'Salvataggio avvenuto con successo';
					}
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					return r;
				}
			);
		}else{
			if (self.upToDate){
				prom = Ac.Db.update('party', dataSet);
				return prom.then(
					function(result){
						r.esito = result.esito;
						if (result.esito) {
							r.messaggio = 'Aggiornamento avvenuto con successo';
						}else{
							r.messaggio = result.messaggio;
						}
						r.data = result.data;
						return r;
					},
					function(error){
						r.esito = false;
						r.messaggio = error.messaggio;
						r.data = error.data;
						return r;
					}
				);
			}else{
				r.esito = true;
				r.messaggio = 'Niente da aggiornare';
				return Promise.resolve(r);
			}
		}
	};

	self.cancella = function(){
		var r = new Response(false, '', this, 'Party.delete');
		if (!Ac.Utils.isValidId(self.id)) {
			r.esito = false;
			r.messaggio = 'Id non valido';
			return Promise.resolve(r);
		}else{
			var q = 'DELETE from party_templates WHERE party_id = '+self.id;
			var delProm = Ac.Db.query(q);
			return delProm.then(
				function(childDelResult){
					return prom = Ac.Db.cancella('party', self.id);
				},
				function(childDelErr){
					_log.error('child delete error');
					return childDelErr;
				}
			).then(
				function(result){
					r.esito = result.esito;
					if (result.esito) {
						r.messaggio = 'Cancellazione avvenuta con successo';
					}else{
						r.messaggio = result.messaggio;
					}
					r.data = result.data;
					return r;
				},
				function(error){
					_log.debug(error);
					r.esito = false;
					r.messaggio = error.messaggio;
					r.data = error.data;
					return r;
				}
			);
		}
	}

	self.addMember = function(Member){
		if (Ac.Utils.isBlank(Member)){
			Ac.Utils.showError('L\'oggetto Membro sembra essere vuoto', Member);
			return;
		} 
		var dataSet;
		var pendingObj = [];
		if (typeof Member != 'object') {
			Ac.Utils.showError('Impossibile aggiungere come Membro questo oggetto', Member);
			return;
		}else{
			if (Member instanceof Template) {
				// _log.debug('add member as template start');
				if (!Ac.Utils.isValidId(Member.id)) {
					Ac.Utils.showError('Impossibile aggiungere come Membro un\'oggetto non salvato', Member);
					return;
				}else{
					pendingObj.push(Member);
					dataSet = {
						party_id: self.id,
						template_id: Member.id
					};
				}
			}else if(Member instanceof Array){
				// _log.debug('add member as array start');
				dataSet = [];
				angular.forEach(Member, function(val, prop){
					if ( !(val instanceof Template) || !Ac.Utils.isValidId(val.id)){
						_log.debug('val is invalid');
					} else {
						pendingObj.push(val);
						var subData = {
							party_id: self.id,
							template_id: val.id
						}
						dataSet.push(subData);
					}
				});
			}else{
				Ac.Utils.showError('Impossibile aggiungere come Membro questo oggetto', Member);
			}
		}
			
		var insProm = Ac.Db.insert('party_templates', dataSet);
		insProm.then(
			function(qRes){
				if (qRes.esito) {
					angular.forEach(pendingObj, function(val, prop){
						self.members.push(val);
					});
					Ac.Utils.showInfo('Inserimento avvenuto con successo!');
				}else{
					Ac.Utils.showWarning(qRes.messaggio);
				}
			},
			function(qErr){
				Ac.Utils.showError(qErr.messaggio, qErr);
			}
		);
	};

	self.removeMember = function(thatMember){
		if (Ac.Utils.isBlank(thatMember)){
			Ac.Utils.showError('L\'oggetto Membro sembra essere vuoto', thatMember);
			return;
		} 
		if (typeof thatMember == 'object') {
			if (thatMember instanceof Template) {
				var idx = self.members.indexOf(thatMember);
				if (idx > -1) {
					self.members.splice(idx, 1);
					var q = 'DELETE from party_templates WHERE party_id = '+self.id+' AND template_id = '+thatMember.id;
					var delProm = Ac.Db.query(q);
					delProm.then(
						function(qRes){
							if (qRes.rowsAffected == 1) {
								Ac.Utils.showInfo('Rimozione avvenuta con successo');
							};
						}, 
						function(qErr){
							Ac.Utils.showError('Errore durante l\'eliminazione dalla tabella di join', qErr);
							return;
						}
					);
				}else{
					Ac.Utils.showError('Membro non trovato dentro questo party', thatMember);
					return;
				}				
				
			}else if(thatMember instanceof Array){
				var q = 'DELETE from party_templates WHERE party_id = '+self.id + ' AND template_id IN (';
				var ids = [];
				for (var i = 0; i < thatMember.length; i++) {
					var member = thatMember[i];
					var idx = self.members.indexOf(member);
					if (idx > -1) {
						self.members.splice(idx, 1);
						ids.push(member.id);
					}else{
						Ac.Utils.showError('Membro non trovato dentro questo party', member);
						continue;
					}
				};
				q += ids.join(', ');
				q += ',-1 )';
				var delProm = Ac.Db.query(q);
				delProm.then(
					function(qRes){
						if (qRes.rowsAffected > 1) {
							Ac.Utils.showInfo('Rimozione avvenuta con successo');
						};
					}, 
					function(qErr){
						Ac.Utils.showError('Errore durante l\'eliminazione dalla tabella di join', qErr);
						return;
					}
				);
			}else{
				Ac.Utils.showError('L\'oggetto Membro non è un oggetto valido', thatMember);
				return;
			}
		}else if (typeof thatMember == 'number' && Ac.Utils.isValidId(thatMember)){
			var member = self.getMember(thatMember);
			var idx = self.members.indexOf(member);
			if (idx > -1) {
				self.members.splice(idx, 1);
				var q = 'DELETE from party_templates WHERE party_id = '+self.id+' AND template_id = '+thatMember.id;
				var delProm = Ac.Db.query(q);
				delProm.then(
					function(qRes){
						if (qRes.rowsAffected == 1) {
							Ac.Utils.showInfo('Rimozione avvenuta con successo');
						};
					}, 
					function(qErr){
						Ac.Utils.showError('Errore durante l\'eliminazione dalla tabella di join', qErr);
						return;
					}
				);
			}else{
				Ac.Utils.showError('Membro non trovato dentro questo party', thatMember);
				return;
			}			
		}else{
			Ac.Utils.showError('L\'oggetto Membro non è un oggetto valido', thatMember);
			return;
		}
	};

	self.getMember = function(memberId){
		if (self.members.length <= 0){return null;};
		var res = Array();
		self.members.filter(function(v, i){
			if (v.id === memberId) {
				res.push(v);
			};
		});
		return res.length !== 1?null:res[0];
	};

	self.hasMember = function(member){
		if (typeof member == 'object' && member instanceof Template) {
			return !Ac.Utils.isBlank(self.getMember(member.id));
		}else if (typeof member == 'number' &&  member>0) {
			return !Ac.Utils.isBlank(self.getMember(member));
		}else if (typeof member == 'string') {
			tmp = parseInt(member, 10);
			if (tmp === NaN) return false;
			return !Ac.Utils.isBlank(self.getMember(tmp));
		}
		return false;

	};

	Object.defineProperty(this, 'countMember', {
		enumerable: false,
		configurable: false,
		get: function() {
			return self.members.length;
		}
	});
}
