function Template(ApplicationContext, row){
	var self = this;
	self.Tb = 'templates';
	
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}

	self.id = -1;
	self.creation = '';
	self.modify = '';
	var holder = {
		name: '',
		type: 0,
		init_mod: 0,
		hp: 0,
		ca: 0,
		note: '',
		typeObj: {}
	};
	self.upToDate = false;

	Object.defineProperty(self, 'name', {
		get: function() {
			return holder.name;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.name = tmp;
			self.upToDate = true;
		}
	});

	Object.defineProperty(self, 'type', {
		get: function() {
			return holder.type;
		},
		set: function(newVal){
			if (!Ac.Utils.isBlank(newVal)){
				if(typeof newVal == 'object' && newVal instanceof TemplateType) {
					holder.typeObj = newVal;
					holder.type = newVal.id
					self.upToDate = true;
				}else if((typeof newVal == 'number' || (typeof newVal == 'string' && parseInt(newVal, 10) != NaN )) && Ac.Utils.isValidId(newVal) ){
					var tmp = parseInt(newVal, 10);
					holder.type = tmp;
					holder.typeObj = new TemplateType(Ac, tmp);
					self.upToDate = true;
				}
			}

		}
	});

	Object.defineProperty(self, 'typeObj', {
		get: function(){
			return holder.typeObj;
		}
	});

	Object.defineProperty(self, 'init_mod', {
		get: function() {
			return holder.init_mod;
		},
		set: function(newVal){
			var tmp;
			if (!Ac.Utils.isBlank(newVal)) {
				tmp = newVal;
				if (typeof tmp != 'number') {
					tmp = parseInt(tmp, 10);
					if (tmp === NaN) tmp = 0;
				}
			}
			holder.init_mod = tmp;
			self.upToDate = true;
		}
	});
	Object.defineProperty(self, 'hp', {
		get: function() {
			return holder.hp;
		},
		set: function(newVal){
			var tmp;
			if (!Ac.Utils.isBlank(newVal)) {
				tmp = newVal;
				if (typeof tmp != 'number') {
					tmp = parseInt(tmp, 10);
					if (tmp === NaN) tmp = 0;
				}
			}
			holder.hp = tmp;
			self.upToDate = true;
		}
	});
	Object.defineProperty(self, 'ca', {
		get: function() {
			return holder.ca;
		},
		set: function(newVal){
			var tmp;
			if (!Ac.Utils.isBlank(newVal)) {
				tmp = newVal;
				if (typeof tmp != 'number') {
					tmp = parseInt(tmp, 10);
					if (tmp === NaN) tmp = 0;
				}
			}
			holder.ca = tmp;
			self.upToDate = true;
		}
	});
	Object.defineProperty(self, 'note', {
		get: function() {
			return holder.note;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal;
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.note = tmp;
			self.upToDate = true;
		}
	});

	self.name;
	self.type;
	self.init_mod;
	self.hp;
	self.ca;
	self.note;

	self.isUsed = false;
	
	if (row !== null){
		if (typeof row === 'number' && Ac.Utils.isValidId(row)) {
			self.id = row;
			delete row;
			var q = 'SELECT * FROM templates WHERE id='+self.id;
			Ac.Db.query(q).then(
				function(result){
					var row = result.rows[0];
					angular.forEach(row, function(val, prop){
						self[prop] = val;
						if (prop == 'type') {
							self.typeObj = new TemplateType(Ac, val);
						};
					}, self);
					var q = "SELECT COUNT(id) AS CNT FROM party_templates WHERE template_id = "+self.id;
					var qProm = Ac.Db.query(q);
					qProm.then(
						function(qRes){
							var cnt = qRes.rows[0].CNT;
							self.isUsed = cnt>0?true:false;
						},
						function(qErr){
							self.isUsed = false;
						}
					);
				},
				function(error){
					console.log(self);
					Ac.Utils.showError('Errore durante il caricamento del tipo template', error);
				}
			);
		}else if(typeof row === 'object'){
			angular.forEach(row, function(val, prop){
				self[prop] = val;
				if (prop == 'type') {
					self.typeObj = new TemplateType(Ac, val);
				};
			}, self);
			var q = "SELECT COUNT(id) AS CNT FROM party_templates WHERE template_id = "+self.id;
			var qProm = Ac.Db.query(q);
			qProm.then(
				function(qRes){
					var cnt = qRes.rows[0].CNT;
					self.isUsed = cnt>0?true:false;
				},
				function(qErr){
					self.isUsed = false;
				}
			);
		}
	}


	self.save = function(){
		var prom;
		var isNew = true;

		var r = new Response(false, '', this, 'Template.save');

		if (this.id > 0) {
			isNew = false;
		}
		if(Ac.Utils.isBlank(holder.name)) {
			r.esito = false;
			r.messaggio = 'Nome mancante';
			return Promise.resolve(r);
		}
		if(Ac.Utils.isBlank(holder.type) || !Ac.Utils.isValidId(holder.type)) {
			r.esito = false;
			r.messaggio = 'Typologia non valida';
			return Promise.resolve(r);
		}
		var dataSet = {
			id: self.id,
			name: self.name,
			type: self.type,
			init_mod: self.init_mod,
			hp: self.hp,
			ca: self.ca,
			note: self.note
		}
		console.log(dataSet);
		if (isNew) {
			prom = Ac.Db.insert('templates', dataSet);
			return prom.then(
				function(result){
					if (result.esito) {
						self.id = result.data.insertId;
						r.esito = true;
						r.messaggio = 'Salvataggio avvenuto con successo';
					}
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					return r;
				}
			);
		}else{
			if (self.upToDate){
				prom = Ac.Db.update('templates', dataSet);
				return prom.then(
					function(result){
						r.esito = result.esito;
						if (result.esito) {
							r.messaggio = 'Aggiornamento avvenuto con successo';
						}else{
							r.messaggio = result.messaggio;
						}
						r.data = result.data;
						return r;
					},
					function(error){
						r.esito = false;
						r.messaggio = error.messaggio;
						r.data = error.data;
						return r;
					}
				);
			}else{
				r.esito = true;
				r.messaggio = 'Niente da aggiornare';
				return Promise.resolve(r);
			}
		}

	};
	self.cancella = function(){
		var r = new Response(false, '', this, 'Template.delete');
		if (!Ac.Utils.isValidId(self.id)) {
			r.esito = false;
			r.messaggio = 'Id non valido';
			return Promise.resolve(r);
		}else{
			var prom = Ac.Db.cancella('templates', self.id);
			return prom.then(
				function(result){
					r.esito = result.esito;
					if (result.esito) {
						r.messaggio = 'Cancellazione avvenuta con successo';
					}else{
						r.messaggio = result.messaggio;
					}
					r.data = result.data;
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					r.data = error.data;
					return r;
				}
			);
		}
	};
}