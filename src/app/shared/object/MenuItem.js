function MenuItem(pName, pIcon, pRoute, pAction){
	var self = this;
	if (typeof pAction != 'function') pAction = undefined;
	var holder = {
		name: '',
		icon: '',
		route: '',
		action: pAction		
	}
	Object.defineProperty(this, 'name', {
		get: function() {
			return holder.name;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || type != 'string') {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.name = tmp;
		}
	});

	Object.defineProperty(this, 'icon', {
		get: function() {
			return holder.icon;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || type != 'string') {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9_]/g, '');
				tmp = tmp.toLowerCase();
			}
			tmp.replace(/ +/g, '_');
			tmp.trim();
			holder.icon = tmp;
		}
	});

	Object.defineProperty(this, 'route', {
		get: function() {
			return holder.route;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || type != 'string') {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9_\/]/g, '');
				tmp = tmp.replace(/.*\.(html|php)/g, '');
			}
			tmp.trim();
			holder.route = tmp;
		}
	});

	Object.defineProperty(this, 'action', {
		get: function(){
			return holder.action
		}
	});

	self.name = pName;
	self.icon = pIcon;
	self.route = pRoute;
	self.action;
}