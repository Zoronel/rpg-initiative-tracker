function TemplateType(ApplicationContext, row){
	var self = this;
	self.Tb = 'tipi_template';
	
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}

	self.id = -1;
	self.creation = '';
	self.modify = '';
	var holder = {
		name: '',
		description: ''
	};
	self.upToDate = false;

	Object.defineProperty(self, 'name', {
		get: function() {
			return holder.name;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.name = tmp;
			self.upToDate = true;
		}
	});

	Object.defineProperty(this, 'description', {
		get: function() {
			return holder.description;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal;
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.description = tmp;
			self.upToDate = true;
		}
	});

	self.name;
	self.description;

	self.isUsed = false;

	if (row !== null){
		if (typeof row === 'number' && Ac.Utils.isValidId(row)) {
			self.id = row;
			delete row;
			var q = 'SELECT * FROM tipi_template WHERE id='+self.id;
			Ac.Db.query(q).then(
				function(result){
					var row = result.rows[0];
					angular.forEach(row, function(val, prop){
						self[prop] = val;
					}, self);
					var q = "SELECT COUNT(id) AS CNT FROM templates WHERE type = "+self.id;
					var qProm = Ac.Db.query(q);
					qProm.then(
						function(qRes){
							var cnt = qRes.rows[0].CNT;
							self.isUsed = cnt>0?true:false;
						},
						function(qErr){
							self.isUsed = false;
						}
					);
				},
				function(error){
					console.log(self);
					Ac.Utils.showError('Errore durante il caricamento del tipo template', error);
				}
			);
		}else if(typeof row === 'object'){
			angular.forEach(row, function(val, prop){
				self[prop] = val;
			}, self);
			var q = "SELECT COUNT(id) AS CNT FROM templates WHERE type = "+self.id;
			var qProm = Ac.Db.query(q);
			qProm.then(
				function(qRes){
					var cnt = qRes.rows[0].CNT;
					self.isUsed = cnt>0?true:false;
				},
				function(qErr){
					self.isUsed = false;
				}
			);
		}
	}

	self.save = function(){
		var prom;
		var isNew = true;

		var r = new Response(false, '', this, 'TemplateType.save');

		if (this.id > 0) {
			isNew = false;
		}
		if(Ac.Utils.isBlank(holder.name)) {
			r.esito = false;
			r.messaggio = 'Nome mancante';
			return Promise.resolve(r);
		}

		var dataSet = {
			id: self.id,
			name: self.name,
			description: self.description
		}

		console.log(dataSet);
		if (isNew) {
			prom = Ac.Db.insert('tipi_template', dataSet);
			return prom.then(
				function(result){
					if (result.esito) {
						self.id = result.data.insertId;
						r.esito = true;
						r.messaggio = 'Salvataggio avvenuto con successo';
					}
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					return r;
				}
			);
		}else{
			if (self.upToDate){
				prom = Ac.Db.update('tipi_template', dataSet);
				return prom.then(
					function(result){
						r.esito = result.esito;
						if (result.esito) {
							r.messaggio = 'Aggiornamento avvenuto con successo';
						}else{
							r.messaggio = result.messaggio;
						}
						r.data = result.data;
						return r;
					},
					function(error){
						r.esito = false;
						r.messaggio = error.messaggio;
						r.data = error.data;
						return r;
					}
				);
			}else{
				r.esito = true;
				r.messaggio = 'Niente da aggiornare';
				return Promise.resolve(r);
			}
		}

	};
	self.cancella = function(){
		var r = new Response(false, '', this, 'TemplateType.delete');
		if (!Ac.Utils.isValidId(self.id)) {
			r.esito = false;
			r.messaggio = 'Id non valido';
			return Promise.resolve(r);
		}else{
			var prom = Ac.Db.cancella('tipi_template', self.id);
			return prom.then(
				function(result){
					r.esito = result.esito;
					if (result.esito) {
						r.messaggio = 'Cancellazione avvenuta con successo';
					}else{
						r.messaggio = result.messaggio;
					}
					r.data = result.data;
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					r.data = error.data;
					return r;
				}
			);
		}
	};
}
