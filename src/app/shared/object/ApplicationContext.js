function ApplicationContext(Db, Utils){
	this.Db = Db;
	this.Utils = Utils;
	this.Core = {};

	this.addCore = function(name, obj){
		if (!this.Core.hasOwnProperty(name)) {
			this.Core[name] = obj;
		}
	};
	this.get = function(name){
		if (this.Core.hasOwnProperty(name)) {
			return this.Core[name];
		};
	};
}