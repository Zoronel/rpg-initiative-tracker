function ContextMenuItem(pName, pIcon, pCode){
	var self = this;
	var holder = {
		name: '',
		icon: '',
		code: ''
	}
	Object.defineProperty(this, 'name', {
		get: function() {
			return holder.name;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || type != 'string') {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.name = tmp;
		}
	});

	Object.defineProperty(this, 'icon', {
		get: function() {
			return holder.icon;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || type != 'string') {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9_]/g, '');
				tmp = tmp.toLowerCase();
			}
			tmp.replace(/ +/g, '_');
			tmp.trim();
			holder.icon = tmp;
		}
	});

	Object.defineProperty(this, 'code', {
		get: function() {
			return holder.code;
		},
		set: function(newVal){
			var tmp;
			var type = typeof newVal;
			if (globalIsblank(newVal) || (type != 'string' && type != 'number')) {
				tmp = '';
			}else{
				tmp = newVal+'';
				tmp = tmp.replace(/[^a-zA-Z0-9]/g, '');
				tmp = tmp.toLowerCase();
			}
			holder.code = tmp;
		}
	});

	self.name = pName;
	self.icon = pIcon;
	self.code = pCode;
}