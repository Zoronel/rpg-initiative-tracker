function Response(esito, messaggio, data, sender){
	
	var holder = {
		esito : false,
		messaggio : '',
		when : new Date(),
		sender : sender,
		data : data
	};

	Object.defineProperty(this, 'esito', {
		get: function() {
			return holder.esito;
		},
		set: function(newVal){
			var tmp;
			if (typeof newVal != 'boolean') {
				if (typeof newVal == 'string') {
					if (newVal.toLowerCase() == 'true' || newVal.toLowerCase() == 'vero' || newVal.toLowerCase() == '1') {
						tmp = true;
					}else if (newVal.toLowerCase() == 'false' || newVal.toLowerCase() == 'falso' || newVal.toLowerCase() == '0') {
						tmp = false;
					}else{
						tmp = false;
					}
				}else if (typeof newVal == 'number') {
					if (newVal == 0) {
						tmp = false;
					}else if (newVal == 1){
						tmp = true;
					}else{
						tmp = false;
					}
				};
			}else{
				tmp = newVal;
			}
			holder.esito = tmp;
		}
	});
	
	this.esito = esito;
	this.messaggio = messaggio;

	Object.defineProperty(this, 'when', {
		enumerable: false,
		configurable: false,
		writable: false,
		value: holder.when
	});
	Object.defineProperty(this, 'sender', {
		enumerable: false,
		configurable: false,
		writable: false,
		value: holder.sender
	});
	Object.defineProperty(this, 'data', {
		get: function() {
			return holder.data;
		},
		set: function(newVal){
			holder.data	 = newVal;
		}
	});
}