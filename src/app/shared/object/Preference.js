function Preference(ApplicationContext, row){
	var self = this;
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}
	var Db = Ac.Db;
	var Utils = Ac.Utils;

	self.id = -1;
	var holder = {
		id: -1,
		field: '',
		value: undefined,
		tipo: undefined,
		editable: true
	}

	Object.defineProperty(self, 'field', {
		get: function () {
			return holder.field;
		},
		set: function(newVal){
			if (!Utils.isBlank(newVal) && angular.isString(newVal) && newVal != holder.field) {
				var tmp = newVal.trim();
				tmp = tmp.replace(/\s+/g, '_');
				holder.field = tmp;
				self.upToDate = true;
			};
		}
	});

	Object.defineProperty(self, 'value', {
		get: function () {
			return holder.value;
		},
		set: function (newVal) {
			if (Utils.isBlank(newVal)) {
				holder.value = null;
			}else{
				if (!Utils.isBlank(holder.tipo)) {
					var colType = holder.tipo;
					var reqType;
					switch(colType){
						default:
						case 'TEXT':
							reqType = 'string';
							break;
						case 'LONG':
						case 'INTEGER':
						case 'NUMBER':
						case 'FLOAT':
							reqType = 'number';
							break;
						case 'BOOLEAN':
							reqType = 'boolean';
							break;
					}
					var valType = typeof newVal;
					if (valType != reqType) {
						if (reqType == 'number' && valType == 'string') {
							newVal = parseInt(newVal, 10);
							if (!isNaN(newVal)) holder.value = newVal;
						}else if (reqType == 'boolean') {
							if (valType == 'string') {
								newVal = newVal.trim();
								newVal = newVal.toLowerCase();
								if (newVal.match(/(true|vero|1){1}/g)) {
									holder.value = true;
									self.upToDate = true;
								}else if (newVal.match(/(false|falso|0){1}/g)) {
									holder.value = false;
									self.upToDate = true;
								}
							}else if (valType == 'number') {
								if (newVal == 0) {
									holder.value = false;
									self.upToDate = true;
								}else if(newVal == 1){
									holder.value = true;
									self.upToDate = true;
								}
							};
						}else if (reqType == 'string') {
							holder.value = newVal + '';
							self.upToDate = true;
						}
					} else{
						holder.value = newVal;
						self.upToDate = true;
					};
				}else{
					holder.value = newVal;
					self.upToDate = true;
				}
			}
		}
	});

	Object.defineProperty(self, 'tipo', {
		get: function(){
			return holder.tipo;
		},
		set: function (newVal) {
			if (!Utils.isBlank(newVal) && angular.isString(newVal) && newVal != holder.tipo){
				newVal = newVal.toUpperCase();
				if(Db.isValidColType(newVal)) {
					holder.tipo = newVal;
					self.upToDate = true;
				}
			};
		}
	});

	Object.defineProperty(self, 'editable', {
		get: function () {
			return holder.editable;
		},
		set: function(newVal){
			var tmp = parseBoolean(newVal);
			if (!angular.isUndefined(tmp) && tmp != holder.editable) {
				holder.editable = tmp;
				self.upToDate = true;
			};
		}
	});

	self.field;
	self.value;
	self.tipo;
	self.editable;

	self.upToDate = false;

	if (row !== null && typeof row === 'object'){
		self.id = row.id;
		self.tipo = row.tipo;
		self.field = row.field;
		self.value = row.value,
		self.editable = row.editable;
	}

	self.save = function(){
		var prom;
		var isNew = true;

		var r = new Response(false, '', self, 'Preference.save');

		if (this.id > 0) {
			isNew = false;
		}
		if (Utils.isBlank(self.field) || Utils.isBlank(self.value)) {
			r.messaggio = 'Campi obbligatori non trovati';
			return Promise.reject(r);
		}else{
			var dataSet = {
				field: self.field,
				value: self.value,
				tipo: self.tipo,
				editable: self.editable
			}
			if (isNew) {
				prom = Db.insert('preferences', 3, dataSet);
				return prom.then(
					function(result){
						if (result.esito) {
							self.id = result.data.insertId;
							r.esito = true;
							r.messaggio = 'Salvataggio avvenuto con successo';
						}
						return r;
					},
					function(error){
						r.esito = false;
						r.messaggio = error.messaggio;
						return r;
					}
				);
			}else{
				if (self.upToDate){
					dataSet.id = self.id;
					prom = Ac.Db.update('preferences', 0, dataSet);
					return prom.then(
						function(result){
							r.esito = result.esito;
							if (result.esito) {
								r.messaggio = 'Aggiornamento avvenuto con successo';
							}else{
								r.messaggio = result.messaggio;
							}
							r.data = result.data;
							return r;
						},
						function(error){
							r.esito = false;
							r.messaggio = error.messaggio;
							r.data = error.data;
							return r;
						}
					);
				}else{
					r.esito = true;
					r.messaggio = 'Niente da aggiornare';
					return Promise.resolve(r);
				}
			}
		}
	}

	self.cancella = function(){
		var r = new Response(false, '', this, 'Party.delete');
		if (!Ac.Utils.isValidId(self.id)) {
			r.esito = false;
			r.messaggio = 'Id non valido';
			return Promise.resolve(r);
		}else{
			var prom = Ac.Db.cancella('party', self.id);
			return prom.then(
				function(result){
					r.esito = result.esito;
					if (result.esito) {
						r.messaggio = 'Cancellazione avvenuta con successo';
					}else{
						r.messaggio = result.messaggio;
					}
					r.data = result.data;
					return r;
				},
				function(error){
					r.esito = false;
					r.messaggio = error.messaggio;
					r.data = error.data;
					return r;
				}
			);
		}
	}
}