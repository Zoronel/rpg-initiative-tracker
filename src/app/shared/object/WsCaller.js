function WsCaller(ApplicationContext){
	var self = this;
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}
	var Db = Ac.Db;
	var Utils = Ac.Utils;
	
	var holder = {
		url: '',
		dataSet: undefined,
		method: 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		transformRequest: function(dataSet) {
			var str = [];
			for(var p in dataSet){
				var val = dataSet[p];
				if (typeof val !== 'object'){
					str.push(encodeURIComponent(p) + '=' + encodeURIComponent(dataSet[p]));
				}else{
					for(var pp in val){
						str.push(encodeURIComponent(p)+'['+encodeURIComponent(pp)+']='+encodeURIComponent(val[pp]) );
					}
				}
			}
			return str.join('&');
		}
	}

	Object.defineProperty(this, 'url', {
		get:function(){
			return holder.url;
		},
		set:function(newVal){
			if (Utils.isBlank(newVal)){
				Utils.showError('Il nuovo valore risulta essere vuoto', newVal);
				return;
			};
			if (typeof newVal != 'string') {
				Utils.showError('Il nuovo valore può essere solo una stringa', newVal);
				return;	
			};
			if (!newVal.match(/^https{0,1}:\/\/[\S]+$/g)) {
				Utils.showError('Il nuovo valore non sembra essere nel formato corretto', newVal);
				return;	
			};
			var tmp = newVal.toLowerCase();
			holder.url = tmp;
		}
	});

	Object.defineProperty(this, 'method', {
		get:function(){
			return holder.method;
		},
		set:function(newVal){
			if (Utils.isBlank(newVal)){
				Utils.showError('Il nuovo valore risulta essere vuoto', newVal);
				return;
			};
			if (typeof newVal != 'string') {
				Utils.showError('Il nuovo valore può essere solo una stringa', newVal);
				return;	
			};
			var tmp = newVal.toUpperCase();
			tmp.trim();
			tmp.replace(/ +/g, '');
			if (!tmp.match(/POST|GET/g)) {
				Utils.showError('Il nuovo valore non è un valore valido. Valori consentiti [POST, GET]', newVal);
				return;	
			};
		}
	});

	Object.defineProperty(this, 'dataSet', {
		get: function(){
			return holder.dataSet;
		},
		set: function(newVal){
			if (Utils.isBlank(holder.dataSet) && !Utils.isBlank(newVal) && typeof newVal == 'object') {
				holder.dataSet = newVal;
			};
		}
	});

	Object.defineProperty(this, 'headers', {
		get: function(){
			return holder.headers;
		}
	});

	Object.defineProperty(this, 'transformRequest', {
		get: function(){
			return holder.transformRequest
		},
		set: function(newVal){
			if (Utils.isBlank(newVal)){
				Utils.showError('Il nuovo valore risulta essere vuoto', newVal);
				return;
			};
			if (typeof newVal != 'function') {
				Utils.showError('Il nuovo valore può essere solo una funzione', newVal);
				return;	
			};
			holder.transformRequest = newVal;
		}
	});

	self.url;
	self.method;
	self.dataSet;
	self.transformRequest;

	self.addData = function(field, value){
		if (Utils.isBlank(field)){
			Utils.showError('E\' necessario indicare il nome del nuovo campo', field);
			return false;
		};
		if (holder.dataSet.hasOwnProperty(field) && !Utils.isBlank(holder.dataSet[field]) ) {
			Utils.showError('Il campo \''+field+'\' è già presente nel dataSet', field);
			return false;
		}; 
		if (Utils.isBlank(value)){
			Utils.showError('Nessun valore riscontrato per il campo \''+field+'\' ', value);
			return false;
		};
		holder.dataSet[field] = value;
		return true;
	}

	self.addNestedData = function(value, field /**/){
		if (Utils.isBlank(field)){
			Utils.showError('E\' necessario indicare il nome del nuovo campo', field);
			return false;
		};
		if (Utils.isBlank(value)){
			Utils.showError('E\' necessario indicare il valore del campo', value);
			return false;
		};
		if (arguments.length == 2){
			return self.addData(field, value);
		}
		var fields = Array.prototype.slice.call(arguments, 1);
		var focus = holder.dataSet;
		var succes = true;
		for (var i = 0; i < fields.length; i++) {
			var nodo = fields[i];
			if (i != (fields.length -1)) {;
				if (!focus.hasOwnProperty(nodo)) {
					focus[nodo] = {};
				};
				focus = focus[nodo];
			}else{
				if (!focus.hasOwnProperty(nodo)) {
					focus[nodo] = value;
				}else{
					Utils.showError('Il campo \''+fields.join(".")+'\' è già presente nel dataSet', fields);
					succes = false;
				}
			}
		};
		return succes;
	}

	self.removeData = function(field){
		if (Utils.isBlank(field)){
			Utils.showError('E\' necessario indicare il nome del valore da togliere', field);
			return false;
		};
		if (!holder.dataSet.hasOwnProperty(field)){
			Utils.showError('Il campo \''+field+'\' non è presente nel dataSet', field);
			return false;
		}
		delete holder.dataSet[field];
		return true;
	}

	self.removeNestedData = function(field /**/){
		if (Utils.isBlank(field)){
			Utils.showError('E\' necessario indicare il nome del valore da togliere', field);
			return false;
		};
		if (Utils.isBlank(value)){
			Utils.showError('E\' necessario indicare il valore del campo', value);
			return false;
		};
		if (arguments.length == 1){
			return self.removeData(field);
		}
		var fields = arguments;
		var focus = holder.dataSet;
		var succes = true;
		for (var i = 0; i < fields.length; i++) {
			var nodo = fields[i];
			if (i != (fields.length -1)) {;
				if (!focus.hasOwnProperty(nodo)) {
					focus[nodo] = {};
				};
				focus = focus[nodo];
			}else{
				if (focus.hasOwnProperty(nodo)) {
					delete focus[nodo];
				}else{
					Utils.showError('Il campo \''+fields.join(".")+'\' non è presente nel dataSet', fields);
					succes = false;
				}
			}
		};
		return succes;
	}

	self.cleanData = function(){
		holder.dataSet = {};
		return true;
	}

	self.send = function(){
		var res = new Response(false, '', null, 'WsCaller.send()');
		if (Utils.isBlank(self.url)) {
			Utils.showError('Nessuna url indicata', url);
			res.message = 'Nessuna url indicata';
			res.data = self;
			Promise.reject(res);
		};
		var wsProm = Utils.wsCall(self);
		return wsProm;
	}
}