function Party(ApplicationContext, row){
	var self = this;
	self.Tb = 'incontro';
	if (angular == null || typeof angular == 'undefined') {
		alert('Angular is undefined');
	};

	var Ac = ApplicationContext;

	if (Ac == null || angular.isUndefined(Ac)) {
		alert('ApplicationContext is undefined!!');
	}

	var _log = Ac.get('$log');

	self.id = -1;
	self.creation = '';
	self.modify = '';

	var holder = {
		id:-1,
		title: ''
	};

	self.upToDate = false;

	Object.defineProperty(this, 'title', {
		get: function() {
			return holder.title;
		},
		set: function(newVal){
			var tmp;
			if (Ac.Utils.isBlank(newVal)) {
				tmp = '';
			}else{
				tmp = newVal.replace(/[^a-zA-Z0-9 .,_]/g, '');
			}
			tmp.replace(/ +/g, ' ');
			tmp.trim();
			holder.title = tmp;
			self.upToDate = true;
		}
	});

	self.members = Array();
	if (row !== null && typeof row === 'object'){
		angular.forEach(row, function(val, prop){
			this[prop] = val;
		}, self);

		angular.forEach(holder, function(val, prop){
			this[prop] = self[prop];
		}, holder);
		
		var memProm = Ac.Db.query('SELECT * FROM membri_incontro WHERE encunter_id = '+self.id);
		memProm.then(
			function(qRes){
				if (qRes.rows.length > 0) {
					_log.debug(self.members);
					for (var i = 0; i < qRes.rows.length; i++) {
						var row = qRes.rows[i];					
						self.members.push(new MemebroIncontro(Ac, row.template_id));
					};
				};
			},
			function(qErr){
				Ac.Utils.showError('Errore durante il recupero dei membri', qErr);			
			}
		);
	}
}