angular.module('app.loader', [
	'ui.router',
])

.config(function config($stateProvider) {
	$stateProvider.state('loader', {
		url: '/loader',
		templateUrl: 'loader/loader.view.html'
	});
});
